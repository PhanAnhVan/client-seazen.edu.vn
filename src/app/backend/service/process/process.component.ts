import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals';
import { TagsService } from '../../../services/integrated/tags.service';
import { uploadFileService } from '../../../services/integrated/upload.service';
import { LinkService } from '../../../services/integrated/link.service';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
})
export class ProcessComponent implements OnInit, OnDestroy {

    fm: FormGroup;

    public connect: any;

    public token: any = {

        process: "set/pages/process",

        pathGetRowPages: "get/pages/getrow",

        pathGetPages: "get/pages/getlist"
    }

    public id: number;

    public flag: boolean = false;

    public listService: any = [];

    public images = new uploadFileService();

    public icons = new uploadFileService();

    constructor(
        public fb: FormBuilder,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals,
        private routerAct: ActivatedRoute,
        public tags: TagsService,
        public link: LinkService
    ) {

        this.routerAct.params.subscribe(params => {
            this.id = +params['id'];
            if (this.id && this.id != 0) {
                this.getRow();
            } else {
                this.fmConfigs()
            }
        })

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case 'GetRowPages':
                    let data = res.data;
                    this.fmConfigs(data);
                    this.getListPages();
                    break;

                case 'PagesProcess':
                    this.showNotification(res);
                    this.flag = false;
                    if (res.status == 1) {
                        setTimeout(() => {
                            this.router.navigate(['admin/service/get-list']);
                        }, 1000);
                    }
                    break;

                case 'getpagesPages':
                    if (res.status == 1) {
                        this.listService = res.data;
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.getListPages();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    getListPages() {
        this.globals.send({ path: this.token.pathGetPages, token: 'getpagesPages', params: { type: 6 } });
    }

    fmConfigs(item: any = "") {

        item = typeof item === 'object' ? item : { status: 1, parent_id: 0, orders: 0, type: 6 };

        this.fm = this.fb.group({

            name: [item.name ? item.name : '', [Validators.required]],

            orders: +item.orders ? +item.orders : 0,

            link: item.link ? item.link : '',
            
            link_redirect: item.link_redirect ? item.link_redirect : '',

            title: item.title ? item.title : '',

            type: item.type ? +item.type : 0,

            parent_id: +item.parent_id ? +item.parent_id : 0,

            detail: item.detail ? item.detail : '',

            description: item.description ? item.description : '',

            keywords: item.keywords ? item.keywords : '',

            status: (item.status && item.status == 1) ? true : false,

            info: item.info ? item.info : '',

        });

        const imagesConfig = { path: this.globals.BASE_API_URL + 'public/pages/', data: item.images ? item.images : '' };
        const iconsConfig = { path: this.globals.BASE_API_URL + 'public/pages/', data: item.icon ? item.icon : '' };

        this.images._ini(imagesConfig);
        this.icons._ini(iconsConfig);

        this.tags._set(item.keywords ? JSON.parse(item['keywords']) : '');
    }

    onChangeLink(e) {
        const url = this.link._convent(e.target.value);
        this.fm.controls['link'].setValue(url);
    }

    getRow() {
        this.globals.send({ path: this.token.pathGetRowPages, token: 'GetRowPages', params: { id: this.id } });
    }

    onSubmit() {

        if (!this.flag) {

            this.flag = true;

            let data: any = this.fm.value;

            data.status = (data.status == true) ? 1 : 0;

            data.keywords = this.tags._get();

            data.link = this.link._convent(data.link);

            data.images = this.images._get(true);

            data.icon = this.icons._get(true);

            this.globals.send({ path: this.token.process, token: 'PagesProcess', data: data, params: { id: this.id || 0 } });
        }
    }

}
