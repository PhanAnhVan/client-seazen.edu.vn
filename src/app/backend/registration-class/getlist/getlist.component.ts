import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core'
import { BsModalService } from 'ngx-bootstrap/modal'
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service'
import { ToastrService } from 'ngx-toastr'
import { Subscription } from 'rxjs'
import { Globals } from '../../../globals'
import { ExcelService } from '../../../services/export/export-excel.service'
import { TableService } from '../../../services/integrated/table.service'
import { AlertComponent } from '../../modules/alert/alert.component'

@Component({
    selector: 'app-getlist-customer',
    templateUrl: './getlist.component.html'
})
export class GetlistComponent implements OnInit, OnDestroy, OnChanges {
    connect: Subscription

    modalRef: BsModalRef

    @Input('filter') filter: any

    cstable = new TableService()

    private token = {
        getlist: 'get/registrationclass/getlist',
        remove: 'set/registrationclass/remove'
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'class.name', field: 'class_name', show: true, filter: true },
        { title: 'sex.title', field: 'sex', show: true, filter: true },
        { title: 'lblPhone', field: 'phone', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: '#', field: 'action', show: true }
    ]

    constructor(
        public globals: Globals,
        private modalService: BsModalService,
        public toastr: ToastrService,
        public ExcelService: ExcelService
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getListStudent':
                    this.cstable._ini({
                        cols: this.cols,
                        data: res.data,
                        count: 50,
                        sorting: { field: 'maker_date', sort: 'DESC', type: 'date' }
                    })

                    break

                case 'removeStudent':
                    let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { timeOut: 1500 })

                    res.status == 1 &&
                        this.globals.send({
                            path: this.token.getlist,
                            token: 'getListStudent'
                        })
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.globals.send({
            path: this.token.getlist,
            token: 'getListStudent'
        })
    }

    ngOnChanges() {
        if (this.filter.class_id) {
            this.globals.send({
                path: this.token.getlist,
                token: 'getListStudent',
                params: {
                    class_id: this.filter.class_id
                }
            })
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    onRemove(student_id: number, class_id: number, name: string) {
        this.modalRef = this.modalService.show(AlertComponent, {
            initialState: { messages: 'student.remove', name: name }
        })
        this.modalRef.content.onClose.subscribe(
            () =>
                student_id > 0 &&
                class_id > 0 &&
                this.globals.send({
                    path: this.token.remove,
                    token: 'removeStudent',
                    params: {
                        student_id: student_id,
                        class_id: class_id
                    }
                })
        )
    }
}
