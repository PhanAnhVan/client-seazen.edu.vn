import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { ServicePipeModule } from '../../services/pipe'
import { GetlistComponent } from './getlist/getlist.component'
import { GroupgetlistComponent } from './groupgetlist/groupgetlist.component'
import { MainComponent } from './main/main.component'
import { ProcessRegistrationClassComponent } from './process/process.component'

const routes: Routes = [
    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: MainComponent },
    { path: 'insert', component: ProcessRegistrationClassComponent },
    { path: 'update', component: ProcessRegistrationClassComponent },
]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        TranslateModule,
        BsDatepickerModule.forRoot(),
        ServicePipeModule
    ],
    declarations: [
        GetlistComponent,
        GroupgetlistComponent,
        ProcessRegistrationClassComponent,
        MainComponent
    ]
})

export class RegistrationClassModule { }