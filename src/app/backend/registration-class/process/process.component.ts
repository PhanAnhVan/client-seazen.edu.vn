import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'
import { TableService } from './../../../services/integrated/table.service'
import { ToslugService } from './../../../services/integrated/toslug.service'

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css'],
    providers: [ToslugService]
})
export class ProcessRegistrationClassComponent implements OnInit, OnDestroy {
    public connect

    fm: FormGroup

    class_id: number = 0
    class_name: string = ''
    student_id: number = 0
    student_name: string = ''
    isExistMessage: string = ''

    cstable = new TableService()

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'student.code', field: 'code', show: true, filter: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'sex.title', field: 'sex', show: true, filter: true },
        { title: 'lblPhone', field: 'phone', show: true },
        { title: 'lbAddress', field: 'address', show: true },
        { title: 'student.code_company', field: 'code_company', show: true, filter: true },
        { title: '', field: 'action', show: true }
    ]

    private token = {
        getAllClass: 'get/registrationclass/getAllClass',
        getAllStudent: 'get/registrationclass/getAllStudent',
        getCourseInClass: 'get/registrationclass/getCourseInClass',
        process: 'set/registrationclass/process',
        getrow: 'get/registrationclass/getrow'
    }
    constructor(
        public fb: FormBuilder,
        public globals: Globals,
        public toastr: ToastrService,
        public router: Router,
        public routerAct: ActivatedRoute
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getrow':
                    if (res.status == 1) {
                        this.class_name = res.data.class_name
                        this.student_name = res.data.student_name

                        // set class to checked by course
                        let value = []
                        res.data.products.filter(item => value.push(item.product_id))
                        this.class.courses.filter(item => (value.includes(item.id) ? (item.isChecked = true) : (item.isChecked = false)))
                    }
                    break

                case 'getAllClass':
                    if (res.status == 1) {
                        this.class.data = res.data
                    }
                    break

                case 'getAllStudent':
                    if (res.status == 1) {
                        this.class.student = res.data

                        this.cstable._ini({
                            cols: this.cols,
                            data: this.class.student,
                            count: 50,
                            sorting: { field: 'code', sort: 'DESC' }
                        })
                    }
                    break

                case 'getCourseInClass':
                    if (res.status == 1) {
                        this.class.courses = res.data
                        if (!this.student_id && !this.class_id) {
                            this.handleCheckAllChange(true)
                        } else {
                            this.globals.send({
                                path: this.token.getrow,
                                token: 'getrow',
                                params: {
                                    class_id: this.class_id,
                                    student_id: this.student_id
                                }
                            })
                        }
                    }
                    break

                case 'process':
                    if (res.status == 1) {
                        this.notificationSuccess(res.message)
                    } else if (res.status == 0) {
                        if (res.data?.length) {
                            this.isExistMessage = res.message

                            // check students who have registered for the course
                            this.class.student.filter(student =>
                                res.data.includes(student.id) ? (student['isExist'] = true) : (student['isExist'] = false)
                            )

                            this.flagSubmit = true
                        } else {
                            this.notificationSuccess(res.message)
                        }
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.routerAct.queryParams.subscribe(params => {
            if (params && params.class_id && params.student_id) {
                this.class_id = this.globals.crypto.decrypt(params.class_id)
                this.student_id = this.globals.crypto.decrypt(params.student_id)
            }

            if (!this.student_id && !this.class_id) {
                // TODO: add
                this.globals.send({
                    path: this.token.getAllClass,
                    token: 'getAllClass'
                })
                this.globals.send({
                    path: this.token.getAllStudent,
                    token: 'getAllStudent'
                })
                this.fmConfigs()
            } else {
                // TODO: update
                this.globals.send({
                    path: this.token.getAllClass,
                    token: 'getAllClass'
                })
                this.fmConfigs({ class_id: this.class_id })
                this.class._choose()
            }
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1 }

        this.fm = this.fb.group({
            class_id: [item.class_id ? +item.class_id : 0, [Validators.required]],
            status: item.status && item.status === 1 ? true : false
        })
    }

    class = {
        data: [],
        courses: [],
        student: [],
        _choose: () => {
            this.globals.send({
                path: this.token.getCourseInClass,
                token: 'getCourseInClass',
                params: { id: this.fm.value.class_id }
            })
        },
        searchCourse: {
            value: '',
            field: ['name']
        },
        searchStudent: {
            value: '',
            field: ['name']
        }
    }

    notificationSuccess(message: string) {
        this.toastr['success'](message, 'success', { timeOut: 1500 })
        setTimeout(() => {
            this.router.navigate(['admin/registration/get-list'])
        }, 1500)
    }

    handleCheckAll() {
        if (this.class.courses?.length) return this.class.courses.every(item => item['isChecked'] === true) ? true : false
    }

    validateSubmit() {
        if (!this.student_id && !this.class_id) {
            if (!this.class.courses?.length || !this.class.student?.length) return true
            return this.class.courses.filter(item => item.isChecked).length && this.class.student.filter(item => item.isChecked).length
                ? false
                : true
        } else {
            if (!this.class.courses?.length) return true
            return this.class.courses.filter(item => item.isChecked).length ? false : true
        }
    }

    handleCheckAllChange = (e: boolean) =>
        this.class.courses.forEach(item => {
            return (item.isChecked = e)
        })

    handleCheckAllStudentsChange = (e: boolean) => this.cstable.data.forEach(item => (item.isChecked = e))

    handleCheckAllStudents = () => (this.cstable.data.every(item => item.isChecked === true) ? true : false)

    flagSubmit: boolean = true
    onSubmit() {
        let course = [],
            student = []
        this.class.courses.filter(item => {
            if (item.isChecked) {
                course.push({ id: item.id, score_pass: item.score_pass })
            }
        })

        if (!this.student_id && !this.class_id) {
            this.class.student.filter(
                item =>
                    item.isChecked &&
                    student.push({
                        class_id: this.fm.value.class_id,
                        student_id: item.id,
                        page_id: this.class.courses[0].page_id,
                        status: 1
                    })
            )
        }

        let item = { course, student }
        
        if (this.flagSubmit) {
            this.globals.send({
                path: this.token.process,
                token: 'process',
                data: item,
                params: {
                    student_id: this.student_id || 0,
                    class_id: this.class_id || this.fm.value.class_id
                }
            })
            
            this.flagSubmit = !this.flagSubmit
        }
    }
}
