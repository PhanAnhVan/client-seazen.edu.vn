import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { TableService } from '../../../services/integrated/table.service';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-groupgetlist',
    templateUrl: './groupgetlist.component.html',
    styleUrls: ['./groupgetlist.component.css']
})

export class GroupgetlistComponent implements OnInit, OnDestroy {

    @Output("filter") filter = new EventEmitter();

    public connect;

    public token: any = {
        getlist: "get/score/getClass",
    }

    private cols = [
        { title: 'score.class', field: 'name', show: true, filter: true },
    ];

    public table = new TableService();

    constructor(
        public globals: Globals
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getClass":
                    this.table._ini({ data: [] });
                    this.table.sorting = { field: "id", sort: "asc", type: 'number' };
                    this.table._concat(res.data, true);
                    break;

                default:
                    break;
            }

        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getlist, token: 'getClass', params: { type: this.globals.USERS.get(true).type, teacher_id: this.globals.USERS.get(true).id } });
        this.table._ini({ cols: this.cols, data: [], keyword: 'class', count: 5 });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    onCheckItem(item) {
        const length = this.table.data.length
        let isEmit = false;

        for (let i = 0; i < length; i++) {
            this.table.data[i].check = this.table.data[i].id == item.id ? (this.table.data[i].check == true ? false : true) : false;

            if (this.table.data[i].check) {
                isEmit = true
            }
        }

        this.filter.emit({ class_id: isEmit ? +item.id : 0 });
    }
}
