import { Component } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { Globals } from '../globals'

@Component({
    selector: 'app-backend',
    templateUrl: './backend.component.html'
})
export class BackendComponent {
    public opened: boolean = true
    public height: number = window.innerHeight

    constructor(public globals: Globals, public translate: TranslateService) {
        this.globals.configCKeditor = Object.assign(this.globals.configCKeditor, this.globals.configUploadCkeditor)

        // TODO: resize fontSize px unit, don't delete these 2 lines
        document.querySelector('html').style.fontSize = 'unset'
        document.querySelector('body').style.fontSize = '1rem'

        this.translate.use('admin')
        if (window['tooltip']) {
            window['tooltip']()
        }
    }

    eventOpened(e: any) {
        this.opened = this.opened == true ? false : true
    }
}
