import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { LinkService } from '../services/integrated/link.service';
import { TableService } from '../services/integrated/table.service';
import { TagsService } from '../services/integrated/tags.service';
import { uploadFileService } from '../services/integrated/upload.service';
import { BackendComponent } from './backend.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AlertComponent } from './modules/alert/alert.component';
import { AsidebarComponent } from './modules/asidebar/asidebar.component';
import { FooterComponent } from './modules/footer/footer.component';
import { HeaderComponent } from './modules/header/header.component';

const appRoutes: Routes = [
    {
        path: '',
        component: BackendComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', component: DashboardComponent },
            { path: 'pages', loadChildren: () => import('./pages/pages.module').then((m) => m.PagesModule) },
            { path: 'products', loadChildren: () => import('./product/product.module').then((m) => m.ProductModule) },
            { path: 'contents', loadChildren: () => import('./contents/contents.module').then((m) => m.ContentsModule) },
            { path: 'contacts', loadChildren: () => import('./contact/contact.module').then((m) => m.ContactModule) },
            { path: 'user', loadChildren: () => import('./personal/personal.module').then((m) => m.PersonalModule) },
            { path: 'setting', loadChildren: () => import('./setting/setting.module').then((m) => m.SettingModule) },
            { path: 'student', loadChildren: () => import('./student/student.module').then((m) => m.StudentModule) },
            { path: 'partner', loadChildren: () => import('./partner/partner.module').then((m) => m.PartnerModule) },
            { path: 'service', loadChildren: () => import('./service/service.module').then((m) => m.ServiceModule) },
            { path: 'evaluate', loadChildren: () => import('./evaluate/evaluate.module').then((m) => m.EvaluateModule) },
            { path: 'document', loadChildren: () => import('./document/document.module').then((m) => m.DocumentModule) },
            { path: 'class', loadChildren: () => import('./class/class.module').then((m) => m.ClassModule) },
            { path: 'enter-score', loadChildren: () => import('./enter-score/enter-score.module').then((m) => m.EnterScoreModule) },
            {
                path: 'registration',
                loadChildren: () => import('./registration-class/registrationClass.module').then((m) => m.RegistrationClassModule),
            },
        ],
    },
];
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule,
        RouterModule.forChild(appRoutes),
        ModalModule.forRoot(),
        TranslateModule,
    ],
    exports: [RouterModule],
    declarations: [AsidebarComponent, BackendComponent, DashboardComponent, HeaderComponent, FooterComponent, AlertComponent],
    providers: [uploadFileService, TagsService, LinkService, TableService],
    entryComponents: [AlertComponent],
})
export class BackendModule {}
