import { Component, OnDestroy, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { BsModalService } from 'ngx-bootstrap/modal'
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'
import { ExcelService } from '../../../services/export/export-excel.service'
import { TableService } from '../../../services/integrated/table.service'
import { AlertComponent } from '../../modules/alert/alert.component'

@Component({
    selector: 'app-getlist-customer',
    templateUrl: './getlist.component.html'
})
export class GetlistComponent implements OnInit, OnDestroy {
    connect

    modalRef: BsModalRef

    cstable = new TableService()

    private token = {
        getlist: 'get/customer/getlist',
        remove: 'set/customer/remove'
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true, filter: true },
        { title: 'student.code', field: 'code', show: true, filter: true },
        { title: 'student.code_company', field: 'code_company', show: true, filter: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblEmail', field: 'email', show: true, filter: true },
        { title: 'lblPhone', field: 'phone', show: true, filter: true },
        { title: '#', field: 'action', show: true }
    ]

    constructor(
        public globals: Globals,
        private modalService: BsModalService,
        public toastr: ToastrService,
        public ExcelService: ExcelService,
        private router: Router
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getlistcustomer':
                    this.cstable._concat(res.data, true)

                    break

                case 'removecustomer':
                    this.showNotification(res)
                    if (res.status == 1) {
                        this.globals.send({
                            path: this.token.getlist,
                            token: 'getlistcustomer'
                        })
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getlist, token: 'getlistcustomer' })

        this.cstable._ini({
            cols: this.cols,
            data: [],
            keyword: 'customer',
            count: 50,
            sorting: { field: 'code', sort: 'DESC' }
        })
    }

    ngOnDestroy() {
        this.connect && this.connect.unsubscribe()
    }

    showNotification(res) {
        let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
        this.toastr[type](res.message, type, { timeOut: 1500 })
    }

    onRemove(id: number, name: any) {
        this.modalRef = this.modalService.show(AlertComponent, {
            initialState: { messages: 'customer.remove', name: name }
        })
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({
                    path: this.token.remove,
                    token: 'removecustomer',
                    params: { id: id || 0 }
                })
            }
        })
    }

    handleExportSampleForm = () => {
        const FILE_NAME = 'FORM MẪU BỘ THƯƠNG BÌNH VÀ XÃ HỘI'
        const colsExport = [
            { title: 'lblStt', field: 'index', show: true },
            { title: 'student.code', field: 'code', show: true },
            { title: 'student.fullName', field: 'name', show: true },
            { title: 'sex.title', field: 'label_sex', show: true },
            { title: 'user.birth_date', field: 'birth_date', show: true, date: true },
            { title: 'student.majors', field: 'majors', show: true },
            { title: 'student.level', field: 'academic_level', show: true },
            { title: 'student.course', field: 'courses', show: true },
            { title: 'student.object_name', field: 'object_name', show: true },
            { title: 'Tình trạng', field: 'status', show: true },
            { title: 'student.nation', field: 'nation', show: true },
            { title: 'student.is_poors', field: 'label_is_poors', show: true },
            { title: 'student.is_poor', field: 'label_is_poor', show: true },
            { title: 'student.is_revolution', field: 'label_is_revolution', show: true },
            { title: 'Ngày cấp văn bằng', field: 'date', show: true }
        ]
        this.cstable.cachedList.filter(item => {
            item.label_sex = +item.sex === 1 ? 'Nam' : +item.sex === 0 ? 'Nữ' : ''
            item.label_is_poors = item.label_is_poor == 1 ? 'Có' : ''
            item.label_is_poor = item.label_is_poor == 0 ? 'Có' : ''
            item.label_is_revolution = item.is_revolution == 1 ? 'Có' : 'Không'
        })

         this.cstable.cachedList.sort((a, b) => {
             if (a.code < b.code) return -1
             if (a.code > b.code) return 1
             return 0
         })

        this.ExcelService.ini(colsExport, this.cstable.cachedList, FILE_NAME)
    }

    handleExportStudentInfo = () => {
        const FILE_NAME = 'Thông tin học viên'
        const colsExport = [
            { title: 'lblStt', field: 'index', show: true },
            { title: 'student.code', field: 'code', show: true },
            { title: 'student.fullName', field: 'name', show: true },
            { title: 'user.avatar', field: 'label_avatar', show: true },
            { title: 'sex.title', field: 'label_sex', show: true },
            { title: 'user.birth_date', field: 'birth_date', show: true, date: true },
            { title: 'student.majors', field: 'majors', show: true },
            { title: 'student.level', field: 'academic_level', show: true },
            { title: 'student.course', field: 'courses', show: true },
            { title: 'student.object_name', field: 'object_name', show: true },
            { title: 'student.height', field: 'height', show: true },
            { title: 'student.weight', field: 'weight', show: true },
            { title: 'lblPhone', field: 'phone', show: true },
            { title: 'lbAddress', field: 'address', show: true },
            { title: 'Facebook', field: 'facebook', show: true },
            { title: 'Điều kiện tốt nghiệp', field: 'condition', show: true },
            { title: 'student.groupCMNN', field: 'label_groupCMNNs', show: true },
            { title: 'student.idno_date', field: 'idno_date', show: true, date: true },
            { title: 'student.idno_place', field: 'idno_place', show: true },
            { title: 'student.groupCMNNs', field: 'groupCMNNs', show: true },
            { title: 'student.excelCertification', field: 'excelCertification', show: true },
            { title: 'student.image', field: 'image', show: true },
            { title: 'student.is_register_form', field: 'label_is_register_form', show: true }
        ]
        this.cstable.cachedList.filter(item => {
            item.label_sex = +item.sex === 1 ? 'Nam' : +item.sex === 0 ? 'Nữ' : ''
            item.label_avatar = ''
            item.label_groupCMNNs = item.idno.length ? 'Có' : 'Không'
            item.label_excelCertification = item.is_certification == 1 ? 'Có' : 'Không'
            item.label_image = item.label_avatar.length ? 'Có' : 'Không'
            item.label_is_register_form = item.is_register_form == 1 ? 'Có' : 'Không'
        })

        this.cstable.cachedList.sort((a, b) => {
            if (a.code < b.code) return -1
            if (a.code > b.code) return 1
            return 0
        })

        this.ExcelService.ini(colsExport, this.cstable.cachedList, FILE_NAME)
    }

    handleImportClick = () => this.router.navigate([`/admin/student/import`])
}
