import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { ToslugService } from 'src/app/services/integrated/toslug.service'
import { Globals } from '../../../globals'
import { uploadFileService } from './../../../services/integrated/upload.service'

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css'],
    providers: [ToslugService]
})
export class ProcessCustomerComponent implements OnInit, OnDestroy {
    public connect

    fm: FormGroup
    public id: number

    largestCode: string = ''
    typesIdNo = [
        { id: 1, value: 'student.CMND' },
        { id: 2, value: 'student.CCCD' },
        { id: 3, value: 'student.passport' }
    ]
    placeholderIdNo: number
    avatar = new uploadFileService()

    private token = {
        getrow: 'get/customer/getrow',
        process: 'set/customer/process',
        getStudentLargestCode: 'get/customer/getStudentLargestCode'
    }

    constructor(
        public fb: FormBuilder,
        public globals: Globals,
        public toastr: ToastrService,
        public router: Router,
        public routerAct: ActivatedRoute
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id'] || 0

            this.id === 0 && (this.placeholderIdNo = this.typesIdNo[0].id)
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getrow':
                    if (res.status == 1) {
                        this.fmConfigs(res.data)
                        this.placeholderIdNo = this.fm.value.type_idno
                    }
                    break

                case 'processCustomer':
                    this.showNotification(res)
                    if (res.status == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/student/get-list'])
                        }, 1000)
                    }
                    break

                case 'getStudentLargestCode':
                    if (res.status !== 1 || res.data.length === 0) {
                        const CREATE_CODE = 'SSE_0001'

                        return this.fmConfigs(CREATE_CODE)
                    }

                    let code = res.data.slice(4)
                    this.largestCode = 'SSE_' + (++code).toLocaleString(undefined, { useGrouping: false, minimumIntegerDigits: 4 })

                    this.fmConfigs(this.largestCode)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        if (this.id && this.id != 0) {
            this.globals.send({
                path: this.token.getrow,
                token: 'getrow',
                params: { id: this.id }
            })
        } else {
            this.globals.send({
                path: this.token.getStudentLargestCode,
                token: 'getStudentLargestCode'
            })
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    showNotification(res) {
        let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
        this.toastr[type](res.message, type, { timeOut: 1500 })
    }

    fmConfigs(item) {
        item = typeof item === 'object' ? item : { code: item, status: 1 }
        const avatarConfig = {
            path: this.globals.BASE_API_URL + 'public/avatar/',
            data: item.avatar ? item.avatar : ''
        }
        this.avatar._ini(avatarConfig)

        this.fm = this.fb.group({
            code: item.code ? item.code : '',
            code_company: item.code_company ? item.code_company : null,
            name: item.name ? item.name : '',
            sex: item.sex ? item.sex : null,
            birth_date: item.birth_date ? new Date(item.birth_date) : null,
            phone: [item.phone ? item.phone : null, [Validators.pattern('^[0-9]*$')]],
            email: [
                item.email ? item.email : null,
                [Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)]
            ],
            type_idno: +item.type_idno ? +item.type_idno : '',
            idno: item.idno ? item.idno : null,
            idno_place: item.idno_place ? item.idno_place : null,
            idno_date: item.idno_date ? new Date(item.idno_date) : null,
            password: [item.password ? item.password : '', [Validators.required, Validators.minLength(8)]],
            academic_level: item.academic_level ? item.academic_level : null,
            address: item.address ? item.address : null,
            facebook: item.facebook ? item.facebook : null,
            weight: item.weight ? item.weight : null,
            height: item.height ? item.height : null,
            object_name: item.object_name ? item.object_name : null,
            nation: item.nation ? item.nation : null,
            is_poor: item.is_poor ? item.is_poor : null,
            is_revolution: +item.is_revolution ? +item.is_revolution : 0,
            is_certification: +item.is_certification ? +item.is_certification : null,
            is_register_form: +item.is_register_form ? +item.is_register_form : null,
            status: +item.status === 1 ? true : false
        })
    }

    onSubmit() {
        const obj = this.fm.value

        obj.avatar = this.avatar._get(true)

        this.globals.send({
            path: this.token.process,
            token: 'processCustomer',
            data: obj,
            params: { id: this.id || 0 }
        })
    }
}
