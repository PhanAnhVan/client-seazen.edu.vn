import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { TimepickerModule } from 'ngx-bootstrap/timepicker'
import { TagsService } from '../../services/integrated/tags.service'
import { EmailComponent } from './email/email.component'
import { OrtherComponent } from './orther/orther.component'
import { SettingComponent } from './setting.component'
import { WebsiteComponent } from './website/website.component'
import { CKEditorModule } from 'ckeditor4-angular'

export const routes: Routes = [
    {
        path: '',
        component: SettingComponent,
        children: [
            { path: '', redirectTo: 'website' },
            { path: 'slide', loadChildren: () => import('./slide/slide.module').then(m => m.SlideModule) },
            { path: 'email', component: EmailComponent },
            { path: 'website', component: WebsiteComponent },
            { path: 'orther', component: OrtherComponent },
            { path: 'company', component: OrtherComponent },
            { path: 'social-network', component: OrtherComponent },
            { path: 'menu', loadChildren: () => import('./menu/menu.module').then(m => m.SettingsMenuModule) },
            { path: 'product', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'content', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            {
                path: 'group-document',
                loadChildren: () => import('./group-document/group-document.module').then(m => m.GroupDocumentModule)
            },
            { path: 'type-document', loadChildren: () => import('./type-document/type-document.module').then(m => m.TypeDocumentModule) },
            { path: 'field', loadChildren: () => import('./field/field.module').then(m => m.FieldModule) },
            { path: 'agencies', loadChildren: () => import('./agencies/agencies.module').then(m => m.AgenciesModule) },
            { path: 'library', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'brand', loadChildren: () => import('./brand/brand.module').then(m => m.SettingBrandModule) },
            { path: 'origin', loadChildren: () => import('./origin/origin.module').then(m => m.OriginModule) },
            { path: 'attribute', loadChildren: () => import('./attribute/attribute.module').then(m => m.SettingAttributeModule) },
            { path: 'link', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'customer', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            {
                path: 'document-product',
                loadChildren: () => import('./document-product/document-product.module').then(m => m.DocumentProductModule)
            },
            { path: 'languages', loadChildren: () => import('./languages/languages.module').then(m => m.LanguagesModule) }
        ]
    }
]
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        CKEditorModule
    ],
    providers: [EmailComponent, WebsiteComponent, OrtherComponent, TagsService],
    declarations: [SettingComponent, EmailComponent, WebsiteComponent, OrtherComponent]
})
export class SettingModule {}
