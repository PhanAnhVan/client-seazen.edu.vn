import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { GetListTypeComponent } from './get-list-type/get-list-type.component';
import { ProcessTypeComponent } from './process-type/process-type.component';

export const routes: Routes = [

    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: GetListTypeComponent },
    { path: 'insert', component: ProcessTypeComponent },
    { path: 'update', component: ProcessTypeComponent },
];
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ],
    declarations: [
        GetListTypeComponent,
        ProcessTypeComponent
    ]
})
export class SetiingsPagesModule { }
