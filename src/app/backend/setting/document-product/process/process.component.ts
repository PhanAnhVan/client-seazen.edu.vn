import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../../globals';
import { TagsService } from '../../../../services/integrated/tags.service';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
})
export class ProcessComponent implements OnInit, OnDestroy {

    fm: FormGroup;

    public id: number = 0;

    public connect;

    public token: any = {
        getrow: "get/productconfig/document/getrow",
        process: "set/productconfig/document/process"
    }

    constructor(
        public fb: FormBuilder,
        public globals: Globals,
        public toastr: ToastrService,
        public router: Router,
        public routerAct: ActivatedRoute,
        public tags: TagsService
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "getrow":
                    let data = res.data;
                    this.fmConfigs(data);
                    break;

                case "processdocument":
                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    if (res.status == 1) {
                        setTimeout(() => {
                            this.router.navigate(['admin/setting/document-product/get-list']);
                        }, 100);
                    }
                    break;

                default:
                    break;
            }
        });
    }
    ngOnInit() {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id'];
            if (this.id && this.id != 0) {
                this.globals.send({
                    path: this.token.getrow,
                    token: 'getrow',
                    params: { id: this.id }
                });
            } else {
                this.fmConfigs();
            }
        });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    fmConfigs(item: any = "") {

        item = typeof item === 'object' ? item : { status: 1 };

        this.fm = this.fb.group({

            name: [item.name ? item.name : '', [Validators.required]],

            description: [item.description ? item.description : ''],

            detail: [item.detail ? item.detail : ''],

            status: (item.status && item.status == 1) ? true : false

        });


        this.tags._set(item.keywords ? JSON.parse(item["keywords"]) : "");
    }

    onSubmit() {

        const obj = this.fm.value;

        obj.keywords = this.tags._get();

        obj.status === true ? obj.status = 1 : obj.status = 0;

        this.globals.send({ path: this.token.process, token: 'processdocument', data: obj, params: { id: this.id || 0 } });
    }
}
