import { Component, OnInit, OnDestroy } from "@angular/core";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { ToastrService } from "ngx-toastr";

import { Globals } from "../../../../globals";
import { TableService } from "../../../../services/integrated/table.service";
import { AlertComponent } from "../../../modules/alert/alert.component";

@Component({
    selector: "app-getlist",
    templateUrl: "./getlist.component.html",
})
export class GetlistComponent implements OnInit, OnDestroy {

    modalRef: BsModalRef;

    public connect;

    public token: any = {
        getlist: "get/productconfig/document/getlist",
        remove: "set/productconfig/document/remove"
    };

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: "lblName", field: "name", show: true, filter: true },
        { title: "lblMaker_date", field: "create_date", show: true, filter: true },
        { title: "#", field: "action", show: true },
        { title: "", field: "status", show: true },
    ];

    public cstable = new TableService();

    constructor(
        public globals: Globals,
        private modalService: BsModalService,
        public toastr: ToastrService
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "getlistdocument":
                    this.cstable.sorting = { field: "maker_date", sort: "DESC", type: "", };
                    this.cstable._concat(res.data, true);
                    break;

                case "removedocument":
                    let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getlist, token: "getlistdocument" });
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getlist, token: "getlistdocument" });

        this.cstable._ini({ cols: this.cols, data: [], keyword: "document", count: 50, });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    onRemove(id: number, name: any) {
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: "documents.remove", name: name } });

        this.modalRef.content.onClose.subscribe((result) => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: "removedocument", params: { id: id } });
            }
        });
    }
}
