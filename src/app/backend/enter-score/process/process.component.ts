import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'
import { ExcelService } from '../../../services/export/export-excel.service'
import { TableService } from '../../../services/integrated/table.service'

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.scss']
})
export class ProcessContentComponent implements OnInit, OnDestroy {
    @Input('product_id') product_id

    @Input('class_id') class_id

    @Input('name') name

    @Input('class_name') class_name

    @Input('lock') lock

    @Input('viewMode') viewMode: boolean

    @Output('change') change = new EventEmitter()

    public connect: any

    isViewMode: boolean

    public token: any = {
        getlist: 'get/score/getListStudent',
        process: 'set/score/process'
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'score.nameStudent', field: 'name', show: true, filter: true },
        { title: 'student.code', field: 'code', show: true, filter: true },
        { title: 'score.attendance', field: 'is_attendance', show: true },
        { title: 'score.score_factor_1', field: 'score_factor_1', show: true },
        { title: 'score.score_factor_2', field: 'score_factor_2', show: true },
        { title: 'score.graduation_condition', field: 'graduation_condition', show: true },
        { title: 'score.discipline', field: 'is_discipline', show: true },
        { title: 'score.score_1', field: 'score_1', show: true },
        { title: 'score.score_2', field: 'score_2', show: true },
        { title: 'score.score_avg', field: 'score_avg', show: true },
        { title: 'score.is_pass', field: 'is_pass', show: true }
    ]

    public cstable = new TableService()

    constructor(
        public globals: Globals,

        public toastr: ToastrService,

        public router: Router,

        public routerAct: ActivatedRoute,

        public ExcelService: ExcelService
    ) {
        this.routerAct.queryParams.subscribe(params => {
            this.product_id = +params['product']
            this.class_id = +params['class']
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getListStudent':
                    if (res.status == 1) {
                        this.score.save = this.lock == 0 ? true : false
                        this.cstable._concat(res.data, true)
                    }
                    break

                case 'processScore':
                    let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { timeOut: 1500 })
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.isViewMode = this.viewMode

        if (this.product_id && this.product_id != 0) {
            this.globals.send({
                path: this.token.getlist,
                token: 'getListStudent',
                params: { class_id: this.class_id, product_id: this.product_id }
            })
            this.cstable._ini({
                cols: this.cols,
                data: [],
                keyword: 'enterScore',
                count: 50,
                sorting: { field: 'code', sort: 'DESC', type: 'date' }
            })
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    public score = {
        save: true,
        _change: (index: number) => {
            this.cstable.data[index].state = true // TODO: Use variable state to select item changed value by index

            this.cstable.data[index].score_factor_1 = handleScore(this.cstable.data[index].score_factor_1)
            this.cstable.data[index].score_factor_2 = handleScore(this.cstable.data[index].score_factor_2)
            this.cstable.data[index].score_1 = handleScore(this.cstable.data[index].score_1)
            this.cstable.data[index].score_2 = handleScore(this.cstable.data[index].score_2)

            function handleScore(score) {
                if (!score) return

                return score > 10 ? '10' : Math.round(score * 100) / 100
            }
        },
        _back: () => this.change.emit(false),
        _clearData: data => {
            data = data.filter((res: any) => {
                // TODO: Get only item changed value by variable state
                if (res.state) {
                    delete res.state
                    delete res.graduation_condition

                    res.note = null // TODO: Don't save data to database
                    res.score = null
                    res.score_pass = null

                    res.score_factor_1 = res.score_factor_1 ? res.score_factor_1 : null
                    res.score_factor_2 = res.score_factor_2 ? res.score_factor_2 : null

                    return res
                }
            })
            return data
        }
    }

    processScore: number
    scoreAVG: number
    isGraduationCondition: number
    isPass: number
    handleGraduationCondition = (attendance, score_factor_1: number, score_factor_2: number, index: number) => {
        if (!attendance && !isNaN(attendance)) return (this.isGraduationCondition = 2)

        if (attendance && +attendance === 1) {
            this.processScore = (score_factor_1 + score_factor_2 * 2) / 3

            const STOP_SCORE = 4

            let result = this.processScore > STOP_SCORE ? true : false

            result ? (this.isGraduationCondition = 1) : (this.isGraduationCondition = 0)

            if (!this.isGraduationCondition) {
                this.cleanByGraduationCondition(index)
            }

            return this.isGraduationCondition
        } else if (attendance && +attendance === 0) {
            this.isGraduationCondition = 0

            this.cleanByGraduationCondition(index)

            return this.isGraduationCondition
        }
    }

    cleanByGraduationCondition = (index: number) => {
        this.cstable.data[index].is_discipline = null
        this.cstable.data[index].score_1 = null
        this.cstable.data[index].score_2 = null
        this.cstable.data[index].score_avg = null
        this.cstable.data[index].is_pass = null

        return this.cstable.data[index]
    }

    handleScoreAVG = (score_1, score_2, index: number) => {
        if (this.isGraduationCondition !== 1) return

        if (!score_1 || isNaN(score_1)) return

        const FINAL_SCORE = score_2 && +score_2 > +score_1 ? score_2 : score_1

        let result = (this.processScore * 40) / 100 + (FINAL_SCORE * 60) / 100

        this.scoreAVG = Math.round(result * 100) / 100

        this.cstable.data[index].score_avg = this.scoreAVG

        return this.scoreAVG
    }

    handlePass = (discipline, score_1, attendance, index: number) => {
        const STOP_SCORE = 5

        if (attendance && +attendance === 0) {
            this.isPass = 0

            this.cstable.data[index].is_pass = this.isPass

            return this.isPass
        }

        if (discipline === null) {
            this.cstable.data[index].is_pass = null

            return (this.isPass = 2)
        }

        if (this.isGraduationCondition === 0 || +discipline === 1) {
            this.isPass = 0

            this.cstable.data[index].is_pass = this.isPass
            
            return this.isPass
        }
        
        if (+discipline === 0 && score_1) {
            const result = this.scoreAVG >= STOP_SCORE ? true : false
            
            this.isPass = result ? 1 : 0
            
            this.cstable.data[index].is_pass = this.isPass
            
            return this.isPass
        } else  {
            this.cstable.data[index].is_pass = null

            return (this.isPass = 2)
        }
    }

    handleDisableScore2 = (score_1, score_2, score_avg, is_pass) => {
        if (this.isGraduationCondition !== 1 || !score_1) return true
        if (score_1 && +score_avg >= 5 && !score_2) return true

        return +score_avg >= 5 && !score_2 ? true : false
    }

    onSubmit() {
        let data = this.score._clearData(this.cstable.data)

        if (!data.length) return

        this.globals.send({ path: this.token.process, token: 'processScore', data: data })
    }

    exportExcel() {
        const cols = [
            { title: 'lblStt', field: 'index', show: true },
            { title: 'score.nameStudent', field: 'name', show: true, filter: true },
            { title: 'score.attendance', field: 'label_is_attendance', show: true },
            { title: 'score.score_factor_1', field: 'score_factor_1', show: true },
            { title: 'score.score_factor_2', field: 'score_factor_2', show: true },
            { title: 'score.graduation_condition', field: 'label_graduation_condition', show: true },
            { title: 'score.discipline', field: 'label_is_discipline', show: true },
            { title: 'score.score_1', field: 'score_1', show: true },
            { title: 'score.score_2', field: 'score_2', show: true },
            { title: 'score.score_avg', field: 'label_score_avg', show: true },
            { title: 'score.is_pass', field: 'label_is_pass', show: true }
        ]

        this.cstable.cachedList.filter(item => {
            item.label_is_attendance =
                +item.is_attendance === 1 ? 'Đạt' : item.is_attendance && +item.is_attendance === 0 ? 'Không đạt' : ''
                
            item.label_is_discipline = +item.is_discipline === 1 ? 'Có' : item.is_discipline && +item.is_discipline === 0 ? 'Không' : ''

            item.label_graduation_condition =
                +item.graduation_condition === 1
                    ? 'Đủ điều kiện'
                    : item.graduation_condition && +item.graduation_condition === 0
                    ? 'Không đủ điều kiện'
                    : ''

            item.label_is_pass = item.score_1 && +item.score_1 >= 0 ? (+item.is_pass ? 'Qua môn' : 'Rớt môn') : ''
            item.label_score_avg = item.score_1 && +item.score_1 >= 0 ? item.score_avg : ''
        })

        const fileName = 'File điểm - ' + this.class_name + ' - ' + this.name
        this.ExcelService.ini(cols, this.cstable.cachedList, fileName)
    }
}
