import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals';
import { AlertComponent } from '../../modules/alert/alert.component';
import { TableService } from '../../../services/integrated/table.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
    styleUrls: ['./getlist.component.css']
})
export class GetlistComponent implements OnInit, OnDestroy {

    public connect: any;

    public id: number;

    public token: any = {
        getlist: "get/partner/getlist",
        remove: "set/partner/remove"
    }

    private cols = [
        { title: "lblStt", field: "index", show: true },
        { title: "partner.logo", field: "logo", show: true },
        { title: "partner.company", field: "company", show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: "lblAction", field: "action", show: true }
    ];

    public cwstable = new TableService();

    private modalRef: BsModalRef;

    constructor(
        public globals: Globals,
        public toastr: ToastrService,
        public modalService: BsModalService,
        public router: Router,
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case 'getlist':
                    this.cwstable.sorting = { field: "maker_date", sort: "DESC", type: "", };
                    this.cwstable._concat(res.data, true);
                    break;

                case 'remove':
                    this.showNotification(res);
                    if (res.status == 1) {
                        this.cwstable._delRowData(this.id);
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        if (this.globals.USERS.get(true).type == 0) {
            this.cwstable._ini({ cols: this.cols, data: [], keyword: 'partner', sorting: { field: "id", sort: "DESC", type: "number" } });
            this.getList();
        } else {
            this.router.navigate([this.globals.admin]);
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getlist' });
    }

    onRemove(id: number, name: any) {
        this.id = id;
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'partner.remove', name: name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'remove', params: { id: id } });
            }
        });
    }
}
