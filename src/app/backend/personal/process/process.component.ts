import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'
import { uploadFileService } from '../../../services/integrated/upload.service'

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css']
})
export class ProcessComponent implements OnInit, OnDestroy {
    private connect

    public fm: FormGroup

    public id: number = 0

    public avatar = new uploadFileService()

    loggedType: number = 0
    loggedId: number = 0
    academics = []

    private token = {
        process: 'set/user/process',
        getrow: 'get/user/getrow',
        getAcademics: 'get/user/academics'
    }

    constructor(
        private fb: FormBuilder,
        private router: Router,
        public globals: Globals,
        private toastr: ToastrService,
        private routerAct: ActivatedRoute
    ) {
        this.loggedType = +this.globals.USERS.get(true).type
        this.loggedId = +this.globals.USERS.get(true).id

        this.routerAct.queryParams.subscribe(params => {
            if (+params.id <= 0 && +params.type <= 0) return
            this.id = +this.globals.crypto.decrypt(params.id)
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getAcademics':
                    this.academics = res.data
                    break

                case 'getrow':
                    this.fmConfigs(res.data)
                    break

                case 'process':
                    this.showNotification(res)
                    if (res.status == 1) {
                        setTimeout(() => {
                            this.router.navigate(['admin/user/get-list'])
                        }, 1000)
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        if (this.id && this.id != 0) {
            this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } })
        } else {
            this.fmConfigs()
        }
        this.globals.send({
            path: this.token.getAcademics,
            token: 'getAcademics'
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    showNotification(res) {
        let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
        this.toastr[type](res.message, type, { timeOut: 1500 })
    }

    processReadonly() {
        if (this.loggedType === 1 || this.loggedId === this.id) {
            return '' // enable readonly
        } else return null // disable
    }

    fmConfigs(data: any = '') {
        data = typeof data === 'object' ? data : { status: 1 }

        const imagesConfig = {
            path: this.globals.BASE_API_URL + 'public/avatar/',
            data: data.avatar ? data.avatar : ''
        }

        this.avatar._ini(imagesConfig)

        this.fm = this.fb.group({
            code: [data.code ? data.code : '', [Validators.required]],

            name: [data.name ? data.name : '', [Validators.required]],

            parent_id: [data.parent_id ? data.parent_id : 0, [Validators.required]],

            birth_date: data.birth_date ? new Date(data.birth_date) : null,

            sex: +data.sex ? +data.sex : '',

            type: +data.type ? +data.type : null,

            format_work: data.format_work ? data.format_work : '',

            academic_level: data.academic_level ? data.academic_level : '',

            teacher: data.teacher ? data.teacher : '',

            phone: [data.phone ? data.phone : '', [Validators.pattern('^[0-9]*$')]],

            email: [
                data.email ? data.email : '',
                [
                    Validators.required,
                    Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)
                ]
            ],

            address: data.address ? data.address : '',

            password: [data.password ? data.password : '', [Validators.required, Validators.minLength(8)]],

            note: data.note ? data.note : '',

            status: data.status && data.status == 1 ? true : false
        })
    }

    onSubmit() {
        let data = this.fm.value

        data.avatar = this.avatar._get(true)

        data.status = data.status == true ? 1 : 0

        this.globals.send({ path: this.token.process, token: 'process', data: data, params: { id: this.id } })
    }
}
