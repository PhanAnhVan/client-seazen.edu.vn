import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { GetlistComponent } from './getlist/getlist.component';
import { ProcessComponent } from './process/process.component';
import { ChangespasswordComponent } from './changespassword/changespassword.component';

const appRoutes: Routes = [
	{ path: '', redirectTo: 'get-list' },
	{ path: 'get-list', component: GetlistComponent },
	{ path: 'insert', component: ProcessComponent },
	{ path: 'update', component: ProcessComponent },
	{ path: 'changespassword', component: ChangespasswordComponent },
]

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild(appRoutes),
		TranslateModule,
		BsDatepickerModule.forRoot(),
	],
	declarations: [
		GetlistComponent,
		ProcessComponent,
		ChangespasswordComponent
	]
})
export class PersonalModule { }
