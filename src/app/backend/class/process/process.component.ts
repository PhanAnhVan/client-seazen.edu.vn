import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../globals';
import { ToslugService } from '../../../services/integrated/toslug.service';
import { AlertComponent } from './../../modules/alert/alert.component';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css'],
    providers: [ToslugService]
})
export class ProcessClassComponent implements OnInit, OnDestroy {
    public connect
    fm: FormGroup
    modalRef: BsModalRef
    public id: number = 0
    isConfirm: boolean = false

    public token: any = {
        getrow: "get/class/getrow",
        process: "set/class/process",
        getMajors: "get/pages/grouptype",

        getCourse: "get/class/getCourse",
        getTeacher: "get/class/getTeacher",
        confirm: "set/class/confirm"
    }
    constructor(
        public fb: FormBuilder,
        public globals: Globals,
        public toastr: ToastrService,
        public router: Router,
        public routerAct: ActivatedRoute,
        private modalService: BsModalService
    ) {
        this.routerAct.queryParams.subscribe(params => {
            if (params && params.id) {
                this.id = +this.globals.crypto.decrypt(params.id);
            }
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getrow":
                    if (res.status == 1) {
                        let data = res.data;
                        this.fmConfigs(data);
                        this.majors.dataCourse = data.course;
                    }
                    break;

                case "getMajors":
                    if (res.status == 1) {
                        this.majors.data = res.data;
                    }
                    break;

                case "getCourse":
                    if (res.status == 1) {
                        this.majors.dataCourse = res.data;
                    }
                    break;

                case "getTeacher":
                    if (res.status == 1) {
                        this.majors.dataTeacher = res.data;
                    }
                    break;

                case 'processClass':
                    this.notification(res)
                    if (res.status === 1) {
                        this.id = res.data
                        setTimeout(() => {
                            this.globals.crypto.link('admin/class/update', { id: this.id })
                        }, 100)
                    }
                    break

                case 'confirm':
                    this.notification(res)
                    res.status == 1 && setTimeout(() => {
                        this.router.navigate(['admin/class/get-list'])
                    }, 1000)
                    break

                default: break
            }
        })
    }

    ngOnInit() {
        if (this.globals.USERS.get(true).type == 0 || this.globals.USERS.get(true).type == 2) {
            if (this.id && this.id != 0) {
                this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
            } else {
                this.fmConfigs()
            }
            this.globals.send({ path: this.token.getTeacher, token: 'getTeacher' });
            this.globals.send({ path: this.token.getMajors, token: 'getMajors', params: { type: 3 } });
        } else {
            this.router.navigate([this.globals.admin]);
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 0, maker_date: new Date() };
        this.fm = this.fb.group({
            code: [item.code ? item.code : '', [Validators.required]],
            name: [item.name ? item.name : '', [Validators.required]],
            page_id: [item.page_id ? item.page_id : '', [Validators.required]],
            total_student: item.total_student ? item.total_student : 0,
            status: item.status ? item.status : 0
        })
    }

    public majors = {
        valueCourse: '',
        field: ['code', 'name'],
        flags: false,
        data: [],
        dataCourse: [],
        dataTeacher: [],
        date: {
            start: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            end: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)
        },
        _choose: () => {
            this.globals.send({ path: this.token.getCourse, token: 'getCourse', params: { id: this.fm.value.page_id } })
        },

        _checkChoose: () => {
            let d = 0;
            for (let i = 0; i < this.majors.dataCourse.length; i++) {
                if (this.majors.dataCourse[i].teacher_id == 0) {
                    this.majors.flags = true;
                    this.majors.dataCourse[i].flags = true;
                } else {
                    d++;
                    this.majors.dataCourse[i].flags = false;
                }
            }
            d == this.majors.dataCourse.length ? this.majors.flags = false : '';
        },
        _searchCourse: () => {

        }
    }

    notification(res: { status: number; message: string }) {
        let type = (res.status == 1) ? 'success' : (res.status == 0 ? 'warning' : 'danger')

        return this.toastr[type](res.message, type, { timeOut: 1500 })
    }

    onSubmit() {
        this.majors._checkChoose();
        if (this.fm.valid && !this.majors.flags) {
            let item = {
                data: this.fm.value,
                dataClassProduct: this.majors.dataCourse,
            }
            this.globals.send({ path: this.token.process, token: 'processClass', data: item, params: { id: this.id || 0 } });
        }
    }

    onConfirm() {
        if (this.id <= 0) return

        this.modalRef = this.modalService.show(
            AlertComponent, {
            initialState: {
                messages: 'class.browse',
                name: this.fm.value.name
            }
        })
        this.modalRef.content.onClose.subscribe(() => this.globals.send({
            path: this.token.confirm,
            token: 'confirm',
            params: { id: this.id }
        }))
    }

    public teacher = {
        valueSearch: '',
        field: ['code', 'name'],

        _select: (item, index) => {
            this.majors.dataCourse[index].teacher_id = item.id;
            this.majors.dataCourse[index].name_teacher = item.name;
            this.teacher._clear();
        },

        _clear: () => {
            this.teacher.valueSearch = '';
        },

        _focusSearch: (id) => {
            setTimeout(() => {
                document.getElementById('teacher' + id).focus();
            }, 300);
        },
    }
}
