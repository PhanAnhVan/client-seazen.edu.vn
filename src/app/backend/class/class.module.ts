import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CKEditorModule } from 'ckeditor4-angular';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ServicePipeModule } from '../../services/pipe';

import { GetlistComponent } from './getlist/getlist.component';
import { ProcessClassComponent } from './process/process.component';
import { ViewStudentComponent } from './view-student/view-student.component';


const appRoutes: Routes = [
	{ path: '', redirectTo: 'get-list' },
	{ path: 'get-list', component: GetlistComponent },
	{ path: 'insert', component: ProcessClassComponent },
	{ path: 'update', component: ProcessClassComponent },
	{ path: 'view-student', component: ViewStudentComponent }
]
@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild(appRoutes),
		BsDatepickerModule.forRoot(),
		TranslateModule,
		CKEditorModule,
		ServicePipeModule
	],
	declarations: [
		GetlistComponent,
		ProcessClassComponent,
		ViewStudentComponent
	]
})
export class ClassModule { }
