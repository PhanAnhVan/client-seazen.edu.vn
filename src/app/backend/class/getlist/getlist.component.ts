import { Component, OnInit, OnDestroy } from '@angular/core'
import { BsModalService } from 'ngx-bootstrap/modal'
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service'
import { ToastrService } from 'ngx-toastr'

import { Globals } from '../../../globals'
import { AlertComponent } from '../../modules/alert/alert.component'
import { TableService } from '../../../services/integrated/table.service'
import { Router } from '@angular/router'

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html'
})
export class GetlistComponent implements OnInit, OnDestroy {
    public connect

    private id: number

    modalRef: BsModalRef

    public token: any = {
        getlist: 'get/class/getlist',
        remove: 'set/class/remove',
        lock: 'set/class/confirm'
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'class.code', field: 'code', show: true, filter: true },
        { title: 'class.name', field: 'name', show: true, filter: true },
        { title: 'class.majors', field: 'majors', show: true, filter: true },
        { title: 'class.number', field: 'total_student', show: true, filter: true },
        { title: 'class.status', field: 'status', show: true },
        { title: '#', field: 'action', show: true }
    ]

    public cstable = new TableService()

    constructor(public globals: Globals, private modalService: BsModalService, public toastr: ToastrService, public router: Router) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getlistclass':
                    this.cstable.sorting = { field: 'maker_date', sort: 'DESC', type: '' }
                    this.cstable._concat(res.data, true)
                    break

                case 'remove':
                    this.showNotification(res)
                    if (res.status == 1) {
                        this.cstable._delRowData(this.id)
                    }
                    break

                case 'lock':
                    this.showNotification(res)
                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getlist, token: 'getlistclass' })
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        if (this.globals.USERS.get(true).type == 0 || this.globals.USERS.get(true).type == 2) {
            this.globals.send({ path: this.token.getlist, token: 'getlistclass' })
            this.cstable._ini({ cols: this.cols, data: [], keyword: 'class', count: 50 })
        } else {
            this.router.navigate([this.globals.admin])
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    showNotification(res) {
        let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
        this.toastr[type](res.message, type, { timeOut: 1500 })
    }

    onRemove(id: number, name: any) {
        this.id = id
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'class.remove', name: name } })
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'remove', params: { id: id } })
            }
        })
    }

    onLock(id: number, is_lock: number, name: string) {
        this.modalRef = this.modalService.show(AlertComponent, {
            initialState: { messages: 'class.lock', name: name }
        })
        this.modalRef.content.onClose.subscribe(() =>
            this.globals.send({
                path: this.token.lock,
                token: 'lock',
                params: {
                    id: id,
                    lock: is_lock == 1 ? 0 : 1
                }
            })
        )
    }
}
