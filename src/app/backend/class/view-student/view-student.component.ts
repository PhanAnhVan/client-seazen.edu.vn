import { Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'
import { TableService } from '../../../services/integrated/table.service'
import { ToslugService } from '../../../services/integrated/toslug.service'

@Component({
    selector: 'app-view-student',
    templateUrl: './view-student.component.html',
    styleUrls: ['./view-student.component.css'],
    providers: [ToslugService]
})
export class ViewStudentComponent implements OnInit, OnDestroy {
    public connect
    public id: number = 0
    public class_name: string = ''
    public product_id: number = 0
    public flag: boolean = false

    public token: any = {
        getListStudent: 'get/class/getListStudent',
        getCourseInClass: 'get/registrationclass/getCourseInClass',
        processVideos: 'set/class/processVideos'
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'student.code', field: 'code', show: true, filter: true },
        { title: 'lblName', field: 'student_name', show: true, filter: true },
        { title: '', field: 'action', show: true }
    ]
    private colsCourse = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true }
    ]

    public cstable = new TableService()
    public cstableCourse = new TableService()

    constructor(public globals: Globals, public toastr: ToastrService, public router: Router, public routerAct: ActivatedRoute) {
        this.routerAct.queryParams.subscribe(params => {
            if (params && params.id) {
                this.id = +this.globals.crypto.decrypt(params.id)
                this.class_name = this.globals.crypto.decrypt(params.class_name)
            }
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getCourse':
                    if (res.status == 1) {
                        this.cstableCourse.data = []
                        this.cstableCourse._concat(res.data, true)
                        this.product_id = res.data[0].id
                        this.globals.send({
                            path: this.token.getListStudent,
                            token: 'getListStudent',
                            params: { class_id: this.id, product_id: res.data[0].id }
                        })
                    }
                    break

                case 'getListStudent':
                    if (res.status == 1) {
                        res.data.filter(item => (item.is_videos = item.is_videos == 0 ? false : true))
                        this.cstable._concat(res.data, true)
                        this.cstable.sorting = { field: 'code', sort: 'DESC', type: '' }
                    }
                    break

                case 'processVideos':
                    this.flag = false
                    let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { timeOut: 1500 })
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        if (this.globals.USERS.get(true).type == 0 || this.globals.USERS.get(true).type == 2) {
            if (this.id && this.id != 0) {
                this.globals.send({ path: this.token.getCourseInClass, token: 'getCourse', params: { id: this.id } })
                this.cstableCourse._ini({ cols: this.colsCourse, data: [], keyword: 'Course', count: 15 })
                this.cstable._ini({
                    cols: this.cols,
                    data: [],
                    keyword: 'listStudent',
                    count: 50
                })
            } else {
                this.router.navigate([this.globals.admin])
            }
        } else {
            this.router.navigate([this.globals.admin])
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    public course = {
        _active: id => {
            this.product_id = id
            this.globals.send({ path: this.token.getListStudent, token: 'getListStudent', params: { class_id: this.id, product_id: id } })
        }
    }

    public student = {
        _changeAll: (e: boolean) => {
            this.cstable.data.filter(item => (item.is_videos = e))
        },

        _checkAll: () => {
            if (this.cstable.data.length) {
                return this.cstable.data.every(item => item['is_videos'] === true) ? true : false
            }
        }
    }

    onSubmit() {
        if (!this.flag) {
            this.flag = true
            this.globals.send({
                path: this.token.processVideos,
                token: 'processVideos',
                data: this.cstable.data,
                params: { class_id: this.id, product_id: this.product_id }
            })
        }
    }

    scrollTop = () => window.scroll(0, 0)
}
