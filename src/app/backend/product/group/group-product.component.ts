import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Globals } from '../../../globals';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-group-product',
    templateUrl: './group-product.component.html',
    styleUrls: ['./group-product.component.css']
})

export class GroupProductGetlistComponent implements OnInit, OnDestroy {

    public connect: any;

    public id;

    @Output("filter") filter = new EventEmitter();

    public token: any = {
        getlist: "get/pages/grouptype"
    }

    private cols = [
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblCount', field: 'count_product', type: 'number', show: true, filter: true }
    ];

    public table = new TableService();

    constructor(
        public globals: Globals
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getlistproductgroup":
                    this.table._concat(res.data, true);
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.getlist();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    getlist() {
        this.globals.send({ path: this.token.getlist, token: 'getlistproductgroup', params: { type: 3 } });
        this.table._ini({ cols: this.cols, data: [], keyword: 'contentgroup', count: 20, sorting: { field: "count_product", sort: "DESC", type: "number" } });
    }

    onCheckItem(item) {
        item.check = (item.check == true) ? false : true;
        this.filter.emit(item.id);
    }
}
