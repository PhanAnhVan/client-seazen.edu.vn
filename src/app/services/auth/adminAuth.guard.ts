import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Globals } from "../../globals";
import { Observable } from 'rxjs';
@Injectable()
export class AdminAuthGuard implements CanActivate {
    constructor(private globals: Globals) { }

    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        return new Observable<boolean>(obs => {
            if (this.globals.USERS.check(true)) {
                this.globals.USERS._check().subscribe((res: any) => {
                    if (res.skip == true) {
                        obs.next(true);
                    } else {
                        this.globals.USERS.remove(true);
                    }
                });
            } else {
                this.globals.USERS.remove(true);
            }
        });
    }
}