import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[symbolInteger]'
})
export class IntegerDirective {

    public element: ElementRef;

    @Input('isEN') isEN;

    constructor(public el: ElementRef) {
        setTimeout(() => {
            this.changes();
        }, 1);
    }

    @HostListener("keyup") keupValue() {
        let regx = this.isEN ? /[^0-9\,]+/g : /[^0-9\.]+/g;
        this.el.nativeElement.value = this.el.nativeElement.value.replace(regx, '');
        this.changes();
    }

    changes = () => {
        let regx = this.isEN ? /\,/g : /\./g;
        let value = this.el.nativeElement.value.toString().replace(regx, '');
        let val = isNaN(value) ? 0 : +value;
        let res = isNaN(value) ? 0 : ((val < 0) ? 0 : value);

        let type = this.isEN ? 'en' : 'vi';
        this.el.nativeElement.value = Number(res).toLocaleString(type);
    }
}