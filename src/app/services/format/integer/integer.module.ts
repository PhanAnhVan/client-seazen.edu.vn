import { NgModule } from "@angular/core";
import { IntegerDirective } from "./integer.directive";

@NgModule({
    declarations: [IntegerDirective],
    exports: [IntegerDirective]
})
export class IntegerModule { }