import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { FormControlName } from '@angular/forms';

@Directive({
    selector: '[symbolDecimal]',
    providers: [FormControlName]
})
export class DecimalDirective {

    @Input('isNegative') isNegative;

    @Input('isEN') isEN;

    constructor(public el: ElementRef, public crl: FormControlName) {

        setTimeout(() => {
            this.changes();
        }, 1);
    }

    @HostListener("keyup") keupValue() {

        let regx = this.isNegative ? /[^0-9\.\,\-]+/g : /[^0-9\.\,]+/g;
        this.el.nativeElement.value = this.el.nativeElement.value.replace(regx, '');
        this.changes();
    }

    changes = () => {

        let negative = this.el.nativeElement.value.charAt(0) == '-' ? true : false;

        let splitBefore = this.isEN ? '.' : ',';
        let split = this.el.nativeElement.value.toString().split(splitBefore);

        let regxBefore = this.isEN ? /\,/g : /\./g;
        let value = split[0].replace(regxBefore, '');

        let regxAfter = this.isEN ? /\./g : /\,/g;
        let type = this.isEN ? 'en' : 'vi';

        let res = isNaN(value) ? 0 : value;
        let result = (negative && this.el.nativeElement.value.length == 1) ? value : Number(res).toLocaleString(type) + (split.length > 1 ? splitBefore + this.el.nativeElement.value.substring(split[0].length + 1, this.el.nativeElement.value.length).replace(regxAfter, '').substring(0, 2) : '');
        if (this.crl['_added']) {
            this.crl.control.setValue(result);
        }
        this.el.nativeElement.value = result;
    }
}
