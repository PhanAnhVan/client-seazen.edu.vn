import { Injectable } from '@angular/core';

@Injectable()
export class RenderHTMLService {
  constructor() {
    // setTimeout(() => {
    //   this.handle;
    // }, 500);
  }

  handle = () => {
    let main = document.getElementById('page_detail');
    if (main) {
      let el = main.querySelectorAll('table');
      if (el) {
        for (let i = 0; i < el.length; i++) {
          let div = document.createElement('div');
          div.className = 'table-responsive table-bordered m-0 border-0';
          el[i].parentNode.insertBefore(div, el[i]);
          el[i].className = 'table';
          el[i].setAttribute('class', 'table');
          let cls = el[i].getAttribute('class');
          el[i];
          let newhtml = "<table class='table'>" + el[i].innerHTML + '</table>';
          el[i].remove();
          div.innerHTML = newhtml;
        }
      }

      let image = main.querySelectorAll('img');
      if (image) {
        for (let i = 0; i < image.length; i++) {
          let a = document.createElement('div');
          image[i].parentNode.insertBefore(a, image[i]);
          let src = image[i].currentSrc;
          let html = `<a data-fancybox class="d-inline-block" href="${src}">${image[i].outerHTML}</a>`;

          image[i].remove();
          a.innerHTML = html;
        }
      }
    }
  };
}
