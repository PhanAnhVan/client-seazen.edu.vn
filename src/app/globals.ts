import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Subject } from 'rxjs'
import { Location } from '@angular/common'

import * as CryptoJS from 'crypto-js'

export class configOptions {
    path: string
    data?: Object
    params?: Object
    token: string
}
@Injectable()
export class Globals {
    // public BASE_API_URL = 'http://localhost/projects/server-seazen.edu.vn/'
    // public BASE_API_URL = 'https://gio21.giowebsite.com/'
    public BASE_API_URL = 'https://seazen.edu.vn/'

    public company: any = {}
    public admin: string = 'admin'
    public debug: Boolean = false
    private response = new Subject<string>()
    public result = this.response.asObservable()

    public configUploadCkeditor = {
        filebrowserBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html?type=Flash',
        filebrowserUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    }

    public configCKeditor = {
        extraPlugins: 'uploadimage,uploadfile,tableresize',
        height: 800,
        contentsCss: ['https://cdn.ckeditor.com/4.16.0/full-all/contents.css', `${this.BASE_API_URL}assets/css/ckeditor.css`],
        bodyClass: 'article-editor,document-editor',
        format_tags: 'p;h1;h2;h3;pre',
        removeDialogTabs: 'image:advanced;link:advanced',
        disallowedContent: 'img{width,height,float}',
        extraAllowedContent: 'img[width,height,align]',
        stylesSet: [
            // Inline Styles
            { name: 'Marker', element: 'span', attributes: { class: 'marker' } },
            { name: 'Cited Work', element: 'cite' },
            { name: 'Inline Quotation', element: 'q' },
            // Object Styles
            {
                name: 'Special Container',
                element: 'div',
                styles: {
                    padding: '5px 10px',
                    background: '#eee',
                    border: '1px solid #ccc'
                }
            },
            {
                name: 'Compact table',
                element: 'table',
                attributes: {
                    cellpadding: '5',
                    cellspacing: '0',
                    border: '1',
                    bordercolor: '#ccc'
                },
                styles: {
                    'border-collapse': 'collapse'
                }
            },
            {
                name: 'Borderless Table',
                element: 'table',
                styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' }
            },
            {
                name: 'Square Bulleted List',
                element: 'ul',
                styles: { 'list-style-type': 'square' }
            }
        ],
        info: {
            toolbar: [
                { name: 'document', items: ['Source'] },
                {
                    name: 'basicstyles',
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript']
                },
                {
                    name: 'paragraph',
                    items: [
                        'NumberedList',
                        'BulletedList',
                        '-',
                        'Blockquote',
                        'CreateDiv',
                        '-',
                        'JustifyLeft',
                        'JustifyCenter',
                        'JustifyRight',
                        'JustifyBlock'
                    ]
                },
                { name: 'links', items: ['Link'] },
                { name: 'insert', items: ['Image'] },
                { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
                { name: 'colors', items: ['TextColor', 'BGColor'] },
                { name: 'tools', items: ['Maximize'] }
            ],
            height: 150
        }
    }

    constructor(private http: HttpClient, public router: Router, public location: Location) {
        this.result.subscribe((response: any) => (this.debug ? console.log(response) : false))
    }

    send = (option: configOptions) => {
        if (option.path && option.token) {
            // check token
            let params = () => {
                let param = '?mask=' + option.token
                if (option.params) {
                    let keys = Object.keys(option.params)
                    for (let i = 0; i < keys.length; i++) {
                        if (i == 0) {
                            param += '&'
                        }
                        param += keys[i] + '=' + option.params[keys[i]]
                        param += i + 1 == keys.length ? '' : '&'
                    }
                }
                return param
            }
            this.http.post(this.BASE_API_URL + option.path + params(), option.data).subscribe((result: any) => {
                this.response.next(result)
            })
        }
    }

    public time = {
        format: e => {
            e = typeof e === 'object' ? e : new Date()
            return e.getFullYear() + '-' + (e.getMonth() + 1).toString() + '-' + e.getDate()
        },
        date: e => {
            if (e) {
                e = typeof e === 'object' ? e : new Date(e)
                return e.getDate() + '/' + (e.getMonth() + 1).toString() + '/' + e.getFullYear()
            }
            return
        },
        calculateDiff: (maker_date: string) => {
            let date = new Date(maker_date)
            let today = new Date()

            let days_ago = (today.getTime() - date.getTime()) / (1000 * 3600 * 24)

            return parseInt(days_ago.toString())
        }
    }

    public formatPrice = price => {
        price = price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        return price
    }

    public USERS = {
        token: 'users',
        store: 'localStorage',
        _check: () => {
            return this.http.post(this.BASE_API_URL + 'api/login/check', this.USERS.get(true))
        },
        get: skip => {
            if (skip == true) {
                return window.localStorage.getItem(this.USERS.store) ? JSON.parse(window.localStorage.getItem(this.USERS.store)) : {}
            } else {
                return window.localStorage.getItem(this.USERS.token) ? window.localStorage.getItem(this.USERS.token) : null
            }
        },
        check: skip => {
            return skip == true
                ? window.localStorage.getItem(this.USERS.store)
                    ? true
                    : false
                : window.localStorage.getItem(this.USERS.token)
                ? true
                : false
        },
        set: (data, skip) => {
            data = typeof data === 'object' ? JSON.stringify(data) : data
            if (skip == true) {
                window.localStorage.setItem(this.USERS.store, data)
            } else {
                window.localStorage.setItem(this.USERS.token, data)
            }
        },
        remove: (skip: any = '') => {
            if (!skip) {
                window.localStorage.removeItem(this.USERS.store)
                // window.localStorage.clear();
            } else {
                window.localStorage.removeItem(this.USERS.token)
            }
            this.router.navigate(['/login/'])

            return this.http.post(this.BASE_API_URL + 'api/logout/admin', {}).subscribe((result: any) => this.response.next(result))
        }
    }

    public CUSTOMER = {
        store: 'localStorageCUSTOMER',
        _check: () => {
            return this.http.post(this.BASE_API_URL + 'api/customer/checkLogin', this.CUSTOMER.get())
        },
        get: () => {
            return window.localStorage.getItem(this.CUSTOMER.store) ? JSON.parse(window.localStorage.getItem(this.CUSTOMER.store)) : {}
        },
        check: () => {
            return window.localStorage.getItem(this.CUSTOMER.store) ? true : false
        },
        set: data => {
            data = typeof data === 'object' ? JSON.stringify(data) : data
            window.localStorage.setItem(this.CUSTOMER.store, data)
        },
        remove: (skip: any = true) => {
            window.localStorage.removeItem(this.CUSTOMER.store)
        }
    }

    public back = () => this.location.back()

    public htmlRender = () => {
        const pageDetail = document.getElementById('page-detail')
        const elm = pageDetail ? pageDetail.querySelectorAll('table') : null
        if (!elm?.length) return

        const elmLength = elm.length
        for (let i = 0; i < elmLength; i++) {
            let container = document.createElement('div')
            container.className = 'table-responsive table-bordered m-0 border-0'
            elm[i].parentNode.insertBefore(container, elm[i])
            elm[i].className = 'table'
            elm[i].setAttribute('class', 'table')
            let rendered = "<table class='table'>" + elm[i].innerHTML + '</table>'
            elm[i].remove()
            container.innerHTML = rendered
        }
    }

    public crypto = {
        secret: 'chips-salt',
        encrypt: value => {
            return encodeURIComponent(CryptoJS.AES.encrypt(value.toString(), this.crypto.secret).toString())
        },
        decrypt: value => {
            value = decodeURIComponent(value)
            return CryptoJS.AES.decrypt(value.toString(), this.crypto.secret).toString(CryptoJS.enc.Utf8)
        },
        create: (link: string, params: any) => {
            let queryParams = ''
            let key = Object.keys(params)

            for (let i = 0; i < Object.keys(params).length; i++) {
                if (i > 0) {
                    queryParams += '&'
                }
                queryParams += key[i] + '=' + this.crypto.encrypt(params[key[i]])
            }

            let base_url = this.BASE_API_URL == 'http://localhost:3000/' ? 'http://localhost:4200/' : this.BASE_API_URL
            return base_url + link + '?' + queryParams
        },
        link: (link: string, params: any) => {
            let key = Object.keys(params)
            for (let i = 0; i < Object.keys(params).length; i++) {
                params[key[i]] = this.crypto.encrypt(params[key[i]])
            }
            this.router.navigate([link], { queryParams: params })
        }
    }
}
