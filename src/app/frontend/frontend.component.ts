import { Component, HostListener, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Globals } from '../globals';

@Component({
    selector: 'app-frontend',
    templateUrl: './frontend.component.html',
    styleUrls: ['./frontend.component.scss'],
})
export class FrontendComponent implements OnInit {
    public connect;
    public utilities: boolean = true;

    chatStatus: boolean = false;

    public path = '';
    public token: any = {
        language: 'api/setting/language',
    };
    constructor(public translate: TranslateService, public router: Router, public globals: Globals) {
        // gọi file lang lên vsf đưa vào vi.json
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case 'language':
                    let data = response.data.reduce((n: { [x: string]: any }, o: { text_key: string | number }) => {
                        if (!n[o.text_key]) {
                            n[o.text_key] = o;
                        }
                        return n;
                    }, []);
                    for (let key in data) {
                        this.translate.set(key, data[key]['value']);
                    }

                    break;
                default:
                    break;
            }
        });

        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                window.scroll(0, 0);
            }
        });
    }
    ngOnInit() {
        this.globals.send({
            path: this.token.language,
            token: 'language',
            params: { status: 1 },
        });
    }

    public windowScrolled: boolean | undefined;

    @HostListener('window: scroll', [])
    onWindowScroll() {
        if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 250) {
            this.windowScrolled = true;
        } else {
            this.windowScrolled = false;
        }
    }

    handleScrollTop() {
        (function smoothscroll() {
            let currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
            if (currentScroll > 0) {
                window.requestAnimationFrame(smoothscroll);
                window.scrollTo(0, currentScroll - currentScroll / 10);
            }
        })();
    }
}
