import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Subscription } from 'rxjs';
import { Globals } from '../../globals';
import { TableService } from '../../services/integrated/table.service';
import { ToslugService } from '../../services/integrated/toslug.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    providers: [ToslugService, TableService],
})
export class SearchComponent implements OnInit, OnDestroy {
    private connect: Subscription;

    initialState: number = -1;

    value: string = '';

    typeSearch: string = '';

    dataNews = [];

    private token = {
        GET_NEWS: 'api/search',
        getContentNews: 'api/content/asidebarRight'
    };

    constructor(private route: ActivatedRoute, public globals: Globals) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getDataSearch':
                    if (res.status == 1) {
                        this.products._ini(res.data.products);
                        this.contents._ini(res.data.contents);
                        this.initialState = (res.data.products.length > 0 || res.data.contents.length) ? 1 : 0;
                    }
                    break;
                case 'getContentNew':
                    if (res.status == 1) {
                        this.dataNews = res.data;
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (Object.keys(params).length > 0) {
                this.typeSearch = params.type || '';
                this.value = params.value || '';
                this.globals.send({ path: this.token.GET_NEWS, token: 'getDataSearch', params: { keywords: this.value } });
                this.typeSearch != '' ? this.globals.send({ path: this.token.getContentNews, token: 'getContentNew', params: { limit: 5, type: 'new' } }) : '';
            }
        });

    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    products = {
        data: [],
        cached: [],
        page: 1,
        total: 0,
        maxSize: 5,
        itemsPerPage: 8,

        _ini: (data) => {
            this.products.data = data;
            this.products.cached = data;
            this.products.total = data.length;
            this.products.data = data.slice(0, this.products.itemsPerPage);
        },

        _change: (e: PageChangedEvent) => {
            const startItem = (e.page - 1) * e.itemsPerPage;

            const endItem = e.page * e.itemsPerPage;

            this.products.data = this.products.cached.slice(startItem, endItem);

            let element = document.getElementById('product-search');

            window.scroll({ top: element.offsetTop - 150, behavior: 'smooth', });
        },

    }

    contents = {
        data: [],
        cached: [],
        page: 1,
        total: 0,
        maxSize: 5,
        itemsPerPage: 8,

        _ini: (data) => {
            this.contents.data = data;
            this.contents.cached = data;
            this.contents.total = data.length;
            this.contents.data = data.slice(0, this.contents.itemsPerPage);
        },

        _change: (e: PageChangedEvent) => {
            const startItem = (e.page - 1) * e.itemsPerPage;

            const endItem = e.page * e.itemsPerPage;

            this.contents.data = this.contents.cached.slice(startItem, endItem);

            let element = document.getElementById('content-search');

            window.scroll({ top: element.offsetTop - 150, behavior: 'smooth', });
        },

    }
    srcoll(idName) {
        var element = document.getElementById(idName);
        window.scroll({ top: element.offsetTop - 250, behavior: 'smooth', });
    }

}
