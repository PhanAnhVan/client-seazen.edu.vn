import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'
import { uploadFileService } from './../../../services/integrated/upload.service'

@Component({
    selector: 'app-info-user',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit, OnDestroy {
    fm: FormGroup
    public connect: any
    public phone: number
    public info: any = {}
    avatar = new uploadFileService()

    public token: any = {
        update: 'api/customer/update'
    }

    constructor(
        public fb: FormBuilder,
        public router: Router,
        public globals: Globals,
        public translate: TranslateService,
        private toastr: ToastrService
    ) {
        this.info = this.globals.CUSTOMER.get()

        Object.keys(this.info).length > 0 ? this.fmConfigs(this.info) : this.fmConfigs()
        const avatarConfig = {
            path: this.globals.BASE_API_URL + 'public/avatar/',
            data: this.info.avatar ? this.info.avatar : ''
        }
        this.avatar._ini(avatarConfig)

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'updateCustomer':
                    let type = response['status'] == 1 ? 'success' : response['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](response['message'], type, { timeOut: 1500 })
                    if (response['status'] == 1) {
                        this.globals.CUSTOMER.set(response['data'])
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {}

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { type: 1, sex: 1 }
        this.fm = this.fb.group({
            id: +item.id ? +item.id : 0,
            email: [
                item.email ? item.email : '',
                [
                    Validators.required,
                    Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)
                ]
            ],
            phone: [item.phone ? item.phone : null, [Validators.pattern('^[0-9]*$')]],
            facebook: item.facebook ? item.facebook : null,
            weight: item.weight ? item.weight : null,
            height: item.height ? item.height : null
        })
    }

    onSubmit() {
        let data = this.fm.value
        data.status = 1

        data.avatar = this.avatar._get(true)

        this.globals.send({ path: this.token.update, token: 'updateCustomer', data: data })
    }
}
