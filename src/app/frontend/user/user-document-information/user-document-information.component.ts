import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Globals } from 'src/app/globals';

@Component({
    selector: 'app-user-document-information',
    templateUrl: './user-document-information.component.html',
    styleUrls: ['./user-document-information.component.scss']
})
export class UserDocumentInformationComponent implements OnInit, OnDestroy {
    private connect: Subscription;

    constructor(
        public globals: Globals,
        private routerAct: ActivatedRoute,
        private _location: Location

    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getDocumentInfo':
                    this.documentInfo.data = res.data;
                    break;
                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.routerAct.params.subscribe(params => {
            this.documentInfo.id = +params.document_id;
            this.documentInfo.send(this.documentInfo.id);
        });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    backClicked() {
        this._location.back();
    }

    public documentInfo = {
        data: <any>{},
        id: 0,
        token: 'api/customer/getDocumentInfo',
        send: (id) => {
            this.globals.send({
                path: this.documentInfo.token,
                token: 'getDocumentInfo',
                params: { document_id: id }
            });
        }
    }
}
