import { Component, OnDestroy, OnInit } from '@angular/core'
import { Globals } from './../../../globals'
import { TableService } from './../../../services/integrated/table.service'

@Component({
    selector: 'app-list-class',
    templateUrl: './list-class.component.html',
    styleUrls: ['./list-class.component.scss']
})
export class ListClassComponent implements OnInit, OnDestroy {
    private connect
    cstable = new TableService()
    isDetail: boolean = false
    detail = {}

    private cols = [
        { title: 'FECustomer.className', field: 'class_name', show: true, filter: true },
        { title: 'FEProduct.product', field: 'product_name', show: true },
        { title: 'FECustomer.attendance', field: 'is_attendance', show: true },
        { title: 'FECustomer.score_factor_1', field: 'score_factor_1', show: true },
        { title: 'FECustomer.score_factor_2', field: 'score_factor_2', show: true },
        { title: 'FECustomer.discipline', field: 'is_discipline', show: true },
        { title: 'FECustomer.score_1', field: 'score_1', show: true },
        { title: 'FECustomer.score_2', field: 'score_2', show: true },
        { title: 'FECustomer.score_avg', field: 'score_avg', show: true },
        { title: 'FECustomer.is_pass', field: 'is_pass', show: true },
        { title: '#', field: 'action', show: true },
        // { title: 'ĐTK *', field: 'majors_score_avg', show: true, filter: true },
        // { title: 'Xếp loại', field: 'classification', show: true, filter: true }
    ]

    constructor(private globals: Globals) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getListClassStudent':
                    if (res.status !== 1 || res.data.length === 0) return

                    this.cstable._ini({
                        cols: this.cols,
                        data: []
                    })
                    this.cstable._concat(res.data, true)
                    break

                case 'getVideos':
                    if (res.status !== 1) return
                    this.isDetail = true
                    this.detail = res.data
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.globals.send({
            path: 'get/list/class/student',
            token: 'getListClassStudent',
            params: { id: +this.globals.CUSTOMER.get().id }
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    handleDetailClick(idProduct) {
        this.globals.send({
            path: 'get/list/videos',
            token: 'getVideos',
            params: { id: idProduct }
        })
    }

    rankStudent: string
    handleRank(scoreAvg: number) {
        if(!scoreAvg) return

        if (scoreAvg >= 9 && scoreAvg < 10) {
            return 'Xuất sắc'
        } else if (scoreAvg >= 8 && scoreAvg < 9) {
            return 'Giỏi'
        } else if (scoreAvg >= 7 && scoreAvg < 8) {
            return 'Khá'
        } else if (scoreAvg >= 6 && scoreAvg < 7) {
            return 'Trung bình Khá'
        } else if (scoreAvg >= 5 && scoreAvg < 6) {
            return 'Trung bình'
        } else {
            return 'Không xác định'
        }
    }
}
