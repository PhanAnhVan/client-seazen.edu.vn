import { UserAuthGuard } from './../../services/auth/userAuth.guard';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { InfoCartComponent } from './cart/cart.component';
import { InfoCartDetailComponent } from './cartdetail/cartdetail.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { CompanyComponent } from '../modules/company/company.component';
import { InfoComponent } from './info/info.component';
import { ListClassComponent } from './list-class/list-class.component';
import { SanitizeHtmlPipe } from './sanitizeHtml.pipe';
import { UserDocumentInformationComponent } from './user-document-information/user-document-information.component';
import { UserProductDetailComponent } from './user-product-detail/user-product-detail.component';
import { UserProductComponent } from './user-product/user-product.component';
import { UserComponent } from './user.component';

const appRoutes: Routes = [
    {
        path: '',
        component: UserComponent,
        canActivate: [UserAuthGuard],
        children: [
            { path: 'hoc-vien', component: InfoComponent },
            { path: 'lop-hoc', component: ListClassComponent },
            { path: 'doi-mat-khau', component: ChangePasswordComponent }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(appRoutes), CommonModule, FormsModule, ReactiveFormsModule, TranslateModule],
    declarations: [
        UserComponent,
        InfoComponent,
        ListClassComponent,
        ChangePasswordComponent,
        UserProductComponent,
        UserProductDetailComponent,
        UserDocumentInformationComponent,
        InfoCartComponent,
        InfoCartDetailComponent,
        SanitizeHtmlPipe,
    ],
})
export class UserModule {}
