import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Globals } from '../../../globals';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-infocartdetail',
    templateUrl: './cartdetail.component.html',
    styleUrls: ['./cartdetail.component.css']
})
export class InfoCartDetailComponent implements OnInit, OnDestroy {

    public token: any = {
        detailcart: "api/customer/ordersdetail"
    }
    public cart: any = {};
    public detail = [];
    public flag = false;
    public connect;
    constructor(public globals: Globals, public routerAct: ActivatedRoute,) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'detailcart':
                    this.cart = response.data.cart;
                    this.detail = response.data.detail;
                    break;

                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.routerAct.params.subscribe((params: any) => {
            this.globals.send({ path: this.token.detailcart, token: 'detailcart', params: { code: params.code } });
        });
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
}