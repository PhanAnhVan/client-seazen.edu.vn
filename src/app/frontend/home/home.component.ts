import { Component, OnDestroy, OnInit } from '@angular/core'
import { Meta, Title } from '@angular/platform-browser'
import { Globals } from 'src/app/globals'
import { DEFAULT_IMAGE } from '../core'
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
    private connect
    private width: number = innerWidth
    defaultImage = DEFAULT_IMAGE

    viewport = {
        desktop: this.width > 768
    }

    private token = {
        SLIDE: 'api/home/slide',
        PRODUCT: 'api/home/getProduct',
        ABOUT: 'api/home/aboutUs',
        NEWS: 'api/home/content',
        PARTNER: 'api/home/getPartner'
    }

    constructor(private globals: Globals, private title: Title, private meta: Meta) {
        this.title.setTitle(this.globals.company.name)
        this.meta.updateTag({
            name: 'description',
            content: this.globals.company.description
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getSlide':
                    this.slide.data = res.data
                    break

                case 'getProductHot':
                    this.product.hot = this.product.compaidData(res.data)
                    break

                case 'getHomeAboutUs':
                    this.about.data = res.data
                    break

                case 'getBanner':
                    this.banner.data = res.data
                    break

                case 'getHomeContent':
                    this.news.data = res.data
                    break

                case 'getPartner':
                    this.partner.list = res.data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.slide.send()
        this.product.send()
        this.about.send()
        this.banner.send()
        this.news.send()
        this.partner.send()
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    slide = {
        lazy_bg: '../../../../../assets/img/lazy-slide-bg.jpg',
        data: [],
        send: () => {
            this.globals.send({
                path: this.token.SLIDE,
                token: 'getSlide',
                params: { type: 1 }
            })
        },
        options: {
            items: 1,
            loop: true,
            autoplay: true,
            nav: true,
            navText: [
                '<img src="../../../../../assets/img/icon-slide-left.png" alt="Previous">',
                '<img src="../../../../../assets/img/icon-slide-right.png" alt="Next">'
            ],
            animateOut: 'fadeOut'
        }
    }

    product = {
        hot: [],
        send: () => {
            this.globals.send({
                path: this.token.PRODUCT,
                token: 'getProductHot',
                params: {
                    type: 'hot',
                    limit: 8
                }
            })
        },
        compaidData: data => {
            let list = []
            if (data && data.length > 0) {
                let length = data.length / 2
                for (let i = 0; i < length; i++) {
                    list[i] = []
                    if (list[i].length < 2) {
                        list[i] = data.splice(0, 2)
                    }
                    data = Object.values(data)
                }
            }
            return list
        }
    }

    about = {
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.token.ABOUT,
                token: 'getHomeAboutUs'
            })
        }
    }

    banner = {
        data: [],
        send: () => {
            this.globals.send({
                path: this.token.SLIDE,
                token: 'getBanner',
                params: { type: 2 }
            })
        },
        options: {
            items: 1,
            loop: true,
            autoplay: true,
            autoplaySpeed: 3300,
            animateOut: 'fadeOut'
        }
    }

    news = {
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.token.NEWS,
                token: 'getHomeContent'
            })
        },
        options: {
            items: this.width > 1024 ? 3 : this.width > 425 ? 2 : 1,
            margin: this.width > 1024 ? 30 : 20,
            stagePadding: 1,
            loop: false,
            rewind: true,
            dots: true,
            nav: false,
            navText: [
                '<img src="../../../../../assets/img/left-arrow.png" alt="Previous">',
                '<img src="../../../../../assets/img/right-arrow.png" alt="Next">'
            ]
        }
    }

    partner = {
        list: [],
        send: () => {
            this.globals.send({
                path: this.token.PARTNER,
                token: 'getPartner'
            })
        },
        options: {
            items: this.width > 1024 ? 5 : this.width > 425 ? 4 : 3,
            margin: this.width > 1024 ? 50 : 25,
            loop: true,
            autoplay: true,
            autoplaySpeed: 3300,
            autoplayHoverPause: true,
            dots: false,
            nav: this.width > 425 ? true : false,
            navText: [
                '<img src="../../../../../assets/img/left-arrow.png" alt="Previous">',
                '<img src="../../../../../assets/img/right-arrow.png" alt="Next">'
            ]
        }
    }
}
