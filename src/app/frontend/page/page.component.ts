import { Component, OnDestroy, OnInit } from '@angular/core'
import { Meta, Title } from '@angular/platform-browser'
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router'
import { Globals } from '../../globals'

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html'
})
export class PageComponent implements OnInit, OnDestroy {
    private connect

    initialState: boolean = true
    data = <any>[]

    constructor(private route: ActivatedRoute, private globals: Globals, private router: Router, private title: Title, private meta: Meta) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'pagesDetail':
                    if (res.status === 1) {
                        this.data = res.data
                        this.title.setTitle(this.data.name)
                        this.meta.updateTag({
                            name: 'description',
                            content: this.data.description
                        })

                        this.initialState = false
                    }
                    
                    
                    else {
                        this.router.navigate(['/404'])
                    }

                    setTimeout(() => {
                        this.globals.htmlRender()
                    }, 500)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let link = params.link || '',
                parentLink = params.parent_link || ''

            this.globals.send({
                path: 'api/pages/detail',
                token: 'pagesDetail',
                params: { link: link, parent_link: parentLink }
            })
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }
}
