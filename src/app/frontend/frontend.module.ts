import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { LazyLoadImageModule } from 'ng-lazyload-image'
import { Ng5SliderModule } from 'ng5-slider'
import { CarouselModule } from 'ngx-bootstrap/carousel'
import { CollapseModule } from 'ngx-bootstrap/collapse'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { BsDropdownModule } from 'ngx-bootstrap/dropdown'
import { ModalModule } from 'ngx-bootstrap/modal'
import { PaginationModule } from 'ngx-bootstrap/pagination'
import { TabsModule } from 'ngx-bootstrap/tabs'
import { TimepickerModule } from 'ngx-bootstrap/timepicker'
import { TypeaheadModule } from 'ngx-bootstrap/typeahead'
import { CarouselModule as OwlCarouselModule } from 'ngx-owl-carousel-o'
import { UserAuthGuard } from '../services/auth/userAuth.guard'
import { BindSrcDirective } from '../services/directive/bindSrc.directive'
import { CartComponent } from './cart/cart.component'
import { ContactComponent } from './contact/contact.component'
import { DetailContentComponent } from './content/detail-content/detail-content.component'
import { ListContentComponent } from './content/list/content.component'
import { CustomerComponent } from './customer/customer.component'
import { DocumentComponent } from './document/document.component'
import { FrontendComponent } from './frontend.component'
import { HomeComponent } from './home/home.component'
import { ModalSinginSingupComponent } from './modal-singin-singup/modal-singin-singup.component'
import { BoxAboutServiceComponent } from './modules/box-about-service/box-about-service.component'
import { BoxCategoryComponent } from './modules/box-category/box-category.component'
import { BoxContentGridComponent } from './modules/box-content-grid/box-content-grid.component'
import { BoxContentComponent } from './modules/box-content/box-content.component'
import { BoxEvaluateComponent } from './modules/box-evaluate/box-evaluate.component'
import { BoxHomeContactComponent } from './modules/box-home-contact/box-home-contact.component'
import { BoxProductComponent } from './modules/box-product/box-product.component'
import { BreadcrumbComponent } from './modules/breadcrumb/breadcrumb.component'
import { CommentComponent } from './modules/comment/comment.component'
import { CompanyComponent } from './modules/company/company.component'
import { FormComponent } from './modules/fm/fm.component'
import { FooterComponent } from './modules/footer/footer.component'
import { HeaderComponent } from './modules/header/header.component'
import { LoadingComponent } from './modules/loading/loading.component'
import { MenuHorizontalComponent } from './modules/menu-horizontal/menu-horizontal.component'
import { MenuComponent } from './modules/menu/menu.component'
import { NotFoundComponent } from './not-found/not-found.component'
import { PageComponent } from './page/page.component'
import { DetailProductComponent } from './product/detail-product/detail-product.component'
import { ListProductComponent } from './product/list/product.component'
import { ResetpasswordComponent } from './resetpassword/resetpassword.component'
import { SanitizeHtmlPipe } from './sanitizeHtml.pipe'
import { SearchComponent } from './search/search.component'
import { SigninComponent } from './signin/signin.component'
import { SignupComponent } from './signup/signup.component'

const appRoutes: Routes = [
    {
        path: '',
        component: FrontendComponent,
        children: [
            { path: 'trang-chu', redirectTo: '' },
            { path: '', component: HomeComponent },
            { path: '404', component: NotFoundComponent },
            { path: 'tim-kiem', component: SearchComponent },
            { path: 'dang-nhap', component: SigninComponent },
            { path: 'tai-lieu', component: DocumentComponent },
            { path: 'doanh-nghiep', component: CompanyComponent },
            {
                path: 'user',
                loadChildren: () => import('./user/user.module').then(m => m.UserModule)
            },
            { path: ':link', component: PageComponent },
            { path: ':parent_link/:link', component: PageComponent },
            { path: ':parent_links/:parent_link/:link', component: DetailContentComponent }
        ]
    },
    { path: '**', redirectTo: '404' }
]

@NgModule({
    imports: [
        LazyLoadImageModule,
        CommonModule,
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        CollapseModule,
        TabsModule.forRoot(),
        RouterModule.forChild(appRoutes),
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        ModalModule.forRoot(),
        CarouselModule,
        Ng5SliderModule,
        OwlCarouselModule,
        TypeaheadModule.forRoot(),
        PaginationModule.forRoot(),
        BsDropdownModule.forRoot()
    ],
    declarations: [
        FrontendComponent,
        HomeComponent,
        ContactComponent,
        HeaderComponent,
        FooterComponent,
        PageComponent,
        MenuComponent,
        MenuHorizontalComponent,
        CommentComponent,
        DetailContentComponent,
        BoxContentComponent,
        BoxContentGridComponent,
        ListContentComponent,
        ListProductComponent,
        SanitizeHtmlPipe,
        BindSrcDirective,
        SearchComponent,
        DetailProductComponent,
        NotFoundComponent,
        BoxProductComponent,
        CustomerComponent,
        SigninComponent,
        SignupComponent,
        FormComponent,
        CartComponent,
        BoxEvaluateComponent,
        ModalSinginSingupComponent,
        BoxCategoryComponent,
        DocumentComponent,
        ResetpasswordComponent,
        BoxAboutServiceComponent,
        BoxHomeContactComponent,
        BreadcrumbComponent,
        LoadingComponent,
        CompanyComponent
    ],
    providers: [UserAuthGuard],
    entryComponents: [ModalSinginSingupComponent]
})
export class FrontendModule {}
