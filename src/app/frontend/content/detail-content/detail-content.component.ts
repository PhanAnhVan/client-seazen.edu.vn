import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Globals } from '../../../globals';
import { ToslugService } from '../../../services/integrated/toslug.service';

@Component({
	selector: 'app-detail-content',
	templateUrl: './detail-content.component.html',
	styleUrls: ['./detail-content.component.scss'],
	providers: [ToslugService],
})
export class DetailContentComponent implements OnInit, OnDestroy {
	private connect: Subscription;

	public data = <any>{};
	public category = [];
	public parent_link: any;
	public link = '';
	public width: number;

	private token: any = {
		GET_NEWS_DETAIL: 'api/content/detail',
	};

	constructor(
		public globals: Globals,
		public fb: FormBuilder,
		public route: ActivatedRoute,
		public router: Router,
		public toSlug: ToslugService,
		private title: Title,
		private meta: Meta
	) {
		this.width = window.innerWidth;
		this.connect = this.globals.result.subscribe((res: any) => {
			switch (res.token) {
				case 'getContentDetail':
					if (res.status === 1 && Object.values(res.data).length) {
						this.data = res.data;

						if (res.data.keywords && res.data.keywords.length > 0) {
							this.data.keywords = JSON.parse(res.data.keywords);
						}
						this.title.setTitle(this.data.name);
						this.meta.updateTag({
							name: 'description',
							content: this.data.description,
						});
						this.meta.updateTag({
							name: 'keywords',
							content: this.data.keywords,
						});

						setTimeout(() => {
							this.globals.htmlRender();
						}, 500);
					} else {
						this.router.navigate(['404']);
					}
					break;
				default:
					break;
			}
		});
	}

	ngOnInit() {
		this.route.params.subscribe((params) => {
			this.link = params.link || '';

			setTimeout(() => {
				this.globals.send({
					path: this.token.GET_NEWS_DETAIL,
					token: 'getContentDetail',
					params: { link: this.link },
				});
			}, 50);
		});

		this.globals.send({
			path: this.token.getContentCategory,
			token: 'getContentCategory',
		});
	}

	ngOnDestroy() {
		this.connect.unsubscribe();
	}

	public search = {
		value: '',

		params: <any>{},

		onSearch: (value: string) => {
			this.search.value = this.toSlug._ini(value.replace(/\s+/g, ' ').trim());
			if (this.search.check_search(this.search.value)) {
				this.search.params.value = this.search.value;
				this.router.navigate(['/tim-kiem'], {
					queryParams: { value: this.search.value, type: 'news' },
					queryParamsHandling: 'merge',
				});
			}
			this.search.value = '';
		},

		check_search: (value: string) => {
			let skip = true;
			skip =
				value.toString().length > 0 &&
				value != '' &&
				value != '-' &&
				value != '[' &&
				value != ']' &&
				value != '\\' &&
				value != '{' &&
				value != '}'
					? true
					: false;
			return skip;
		},
	};
}
