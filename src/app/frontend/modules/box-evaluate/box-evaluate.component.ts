import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-box-evaluate',
  templateUrl: './box-evaluate.component.html',
  styleUrls: ['./box-evaluate.component.scss'],
})
export class BoxEvaluateComponent implements OnInit {
  @Input('item') item: any;

  public width = window.innerWidth;

  constructor() {}

  ngOnInit() {}
}
