import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core'
import { NavigationEnd, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { Subscription } from 'rxjs'
import { Globals } from 'src/app/globals'
import { ModalSinginSingupComponent } from '../../modal-singin-singup/modal-singin-singup.component'

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnChanges, OnDestroy {
    public connect: Subscription
    public type = 'password'
    public policy: any = { data: [], show: false }
    public product: any = { data: [], show: false }
    public services: any = { data: [], show: false }
    public menu: any = { data: [], show: false }
    public payment: any = { data: [], DMCA: {}, MOIT: {} }
    public width: number = 0
    public token: any = { menu: 'api/getmenu' }
    public pageType: number = 0
    public pageCart: string = '/gio-hang'
    public pageCartStatus: boolean
    public modalRef: BsModalRef

    constructor(public globals: Globals, public translate: TranslateService, public router: Router, public modalService: BsModalService) {
        this.width = window.innerWidth
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.pageCart === event.url ? (this.pageCartStatus = false) : (this.pageCartStatus = true)

                this.pageType = 0
            }
        })
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'menuFooterProducts':
                    if (res.status == 1) {
                        this.product.data = res.data
                    }
                    break
                case 'menuFooterService':
                    if (res.status == 1) {
                        this.services.data = res.data
                    }
                    break
                case 'menuFooterPolicy':
                    if (res.status == 1) {
                        this.policy.data = res.data
                    }
                    break
                case 'menuFooterMenu':
                    if (res.status == 1) {
                        this.menu.data = res.data
                    }
                    break

                case 'menuFooterPayment':
                    res.data.map(e => {
                        switch (e.link) {
                            case 'bo-cong-thuong':
                                this.payment.MOIT = e
                                break

                            case 'dmca':
                                this.payment.DMCA = e
                                break

                            default:
                                break
                        }
                    })
                    this.payment.data = res.data
                    break

                case 'pagesdetail':
                    if (res.status == 1 && +res.data.type > 0) {
                        this.pageType = res.data.type
                    }
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        setTimeout(() => {
            this.globals.send({
                path: this.token.menu,
                token: 'menuFooterPolicy',
                params: { position: 'policy' }
            })
            this.globals.send({
                path: this.token.menu,
                token: 'menuFooterProducts',
                params: { position: 'products' }
            })
            this.globals.send({
                path: this.token.menu,
                token: 'menuFooterMenu',
                params: { position: 'end' }
            })
            this.globals.send({
                path: this.token.menu,
                token: 'menuFooterPayment',
                params: { position: 'payment' }
            })

            this.globals.send({
                path: this.token.menu,
                token: 'menuFooterService',
                params: { position: 'services' }
            })
        }, 100)
    }

    ngOnChanges = () => (this.pageType = 0)

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    getType = (link: string, type: string | number) => {
        switch (+type) {
            case 1:
            case 2:
            case 3:
            case 4:
                link = link
                break
            default:
                break
        }
        return link
    }

    routerContact = e => this.router.navigateByUrl('/lien-he', { state: { type: e.target.value } })

    enableFormRegisterCourse = (type: number) =>
        (this.modalRef = this.modalService.show(ModalSinginSingupComponent, {
            class: 'gray modal-md',
            initialState: { data: type }
        }))
}
