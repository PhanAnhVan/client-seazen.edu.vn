import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-menu-horizontal',
    templateUrl: './menu-horizontal.component.html',
    styleUrls: ['./menu-horizontal.component.scss'],
})
export class MenuHorizontalComponent implements OnInit {
    @Input('input') menu: any;

    constructor() {}

    ngOnInit() {}
}
