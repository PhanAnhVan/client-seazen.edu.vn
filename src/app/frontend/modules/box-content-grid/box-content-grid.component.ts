import { Component, Input } from '@angular/core'
import { DEFAULT_IMAGE } from '../../core'

@Component({
    selector: 'box-content-grid',
    templateUrl: './box-content-grid.component.html',
    styleUrls: ['./box-content-grid.component.scss']
})
export class BoxContentGridComponent {
    @Input('input') item
    defaultImage = DEFAULT_IMAGE

    constructor() {}

    ngOnInit() {}
}
