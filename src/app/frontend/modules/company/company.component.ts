import { Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Globals } from './../../../globals'
import { TableService } from './../../../services/integrated/table.service'

@Component({
    selector: 'app-company',
    templateUrl: './company.component.html',
    styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit, OnDestroy {
    private connect

    studentName: string = ''
    isShowDetail: boolean = false
    isLogin: boolean = false

    cstable = new TableService()
    cstableDetail = new TableService()
    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'FECustomer.code', field: 'student_code', show: true, filter: true },
        { title: 'FECustomer.name', field: 'student_name', show: true, filter: true },
        { title: 'FECustomer.product', field: 'product_name', show: true, filter: true },
        { title: 'FECustomer.class', field: 'class_name', show: true, filter: true },
        { title: 'FECustomer.scoreAvg', field: 'score_avg', show: true, filter: true },
        { title: 'FECustomer.rank', field: 'student_rank', show: true },
        { title: '#', field: 'action', show: true }
    ]
    private colsDetail = [
        { title: 'FEProduct.product', field: 'product_name', show: true },
        { title: 'FECustomer.className', field: 'class_name', show: true, filter: true },
        { title: 'FECustomer.attendance', field: 'is_attendance', show: true },
        { title: 'FECustomer.score_factor_1', field: 'score_factor_1', show: true },
        { title: 'FECustomer.score_factor_2', field: 'score_factor_2', show: true },
        { title: 'FECustomer.discipline', field: 'is_discipline', show: true },
        { title: 'FECustomer.score_1', field: 'score_1', show: true },
        { title: 'FECustomer.score_2', field: 'score_2', show: true },
        { title: 'FECustomer.score_avg', field: 'score_avg', show: true },
        { title: 'FECustomer.is_pass', field: 'is_pass', show: true },
        { title: 'FECustomer.rank', field: 'student_rank', show: true }
    ]

    constructor(private globals: Globals, private routerAct: ActivatedRoute, private toastr: ToastrService) {
        this.routerAct.queryParams.subscribe(params => {
            let codeCompany = this.globals.crypto.decrypt(params.code)
            codeCompany.length ? (this.isLogin = true) : (this.isLogin = false)

            if (!codeCompany?.length) return

            this.globals.send({
                path: 'api/customer/getAllStudentByCompany',
                token: 'getAllStudentByCompany',
                params: { code: codeCompany }
            })
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getAllStudentByCompany':
                    if (!this.isLogin) {
                        this.showNotification(res.status, res.message)

                        if (res.status !== 1) return

                        this.globals.crypto.link('doanh-nghiep', { code: res.data })
                    } else {
                        if (res.status !== 1) return

                        this.cstable._ini({
                            cols: this.cols,
                            data: res.data,
                            count: 50,
                            sorting: { field: 'student_code', sort: 'DESC' }
                        })
                    }
                    break

                case 'getListClass':
                    if (res.status !== 1) return

                    this.cstableDetail._ini({
                        cols: this.colsDetail,
                        data: res.data
                    })

                    this.isShowDetail = true
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {}

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    handleStudentClick(id: number, name: string, productId: number) {
        this.studentName = name

        this.globals.send({
            path: 'get/list/class',
            token: 'getListClass',
            params: { id: id, product_id: productId }
        })
    }

    showNotification(status: number, message: string) {
        const type = status === 1 ? 'success' : status === 0 ? 'warning' : 'danger'

        return this.toastr[type](message, type, { timeOut: 1500 })
    }

    company = {
        value: '',
        onSubmit: () => {
            if (!this.company.value.length) return

            this.globals.send({
                path: 'api/customer/companySignin',
                token: 'getAllStudentByCompany',
                params: { code: this.company.value }
            })

            this.company.value = ''
        }
    }

    rankStudent: string
    handleRank(scoreAvg: number) {
        if (scoreAvg >= 9 && scoreAvg < 10) {
            return (this.rankStudent = 'Xuất sắc')
        } else if (scoreAvg >= 8 && scoreAvg < 9) {
            return (this.rankStudent = 'Giỏi')
        } else if (scoreAvg >= 7 && scoreAvg < 8) {
            return (this.rankStudent = 'Khá')
        } else if (scoreAvg >= 6 && scoreAvg < 7) {
            return (this.rankStudent = 'Trung bình Khá')
        } else if (scoreAvg >= 5 && scoreAvg < 6) {
            return (this.rankStudent = 'Trung bình')
        } else {
            return (this.rankStudent = '')
        }
    }
}
