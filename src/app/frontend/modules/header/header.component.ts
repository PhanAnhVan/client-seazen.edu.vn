import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { Globals } from 'src/app/globals'
import { CartService } from 'src/app/services/apicart/cart.service'
import { ToslugService } from 'src/app/services/integrated/toslug.service'
import { ModalSinginSingupComponent } from '../../modal-singin-singup/modal-singin-singup.component'

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    providers: [ToslugService, CartService]
})
export class HeaderComponent implements OnInit, OnDestroy {
    public connect

    menu: any = []

    width: number = innerWidth

    status_menu: boolean = false

    student = {}

    public categories: any
    public modalRef: BsModalRef
    @ViewChild('inputSearch') inputSearch: ElementRef

    codeCompany: string = ''

    public token = {
        menu: 'api/getmenu',
        productCategory: 'api/productCategory'
    }

    constructor(
        public globals: Globals,
        public translate: TranslateService,
        public router: Router,
        public toSlug: ToslugService,
        public routerAtc: ActivatedRoute,
        public cartApi: CartService,
        public modalService: BsModalService
    ) {
        this.student = globals.CUSTOMER.get()
        if (window['scrollMenu']) {
            window['scrollMenu']()
        }

        this.router.events.subscribe(e => {
            if (e instanceof NavigationEnd) {
                this.search.icon = false
            }
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getMainMenu':
                    this.menu = this.compaid(res.data)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.globals.send({
            path: this.token.menu,
            token: 'getMainMenu',
            params: { position: 'menuMain' }
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    compaid(data: any[]) {
        let list = []
        data = data.filter(function (item: { parent_id: string | number }) {
            let v = isNaN(+item.parent_id) && item.parent_id ? 0 : +item.parent_id
            v == 0 ? '' : list.push(item)
            return v == 0 ? true : false
        })
        let compaidmenu = (data: string | any[], skip: boolean, level = 0) => {
            level = level + 1
            if (skip == true) {
                return data
            } else {
                for (let i = 0; i < data.length; i++) {
                    let obj = data[i]['data'] && data[i]['data'].length > 0 ? data[i]['data'] : []
                    list = list.filter(item => {
                        let skip = +item.parent_id == +data[i]['id'] ? false : true
                        if (skip == false) {
                            obj.push(item)
                        }
                        return skip
                    })
                    let skip = obj.length == 0 ? true : false
                    data[i]['level'] = level
                    data[i]['href'] = data[i]['link']
                    data[i]['data'] = level >= 2 ? [] : compaidmenu(obj, skip, level)
                }
                return data
            }
        }
        return compaidmenu(data, false)
    }

    _closeMenu = e => (this.status_menu = e)

    logout() {
        this.globals.CUSTOMER.remove(true)
        this.router.navigate(['/'])
    }

    login = type =>
        (this.modalRef = this.modalService.show(ModalSinginSingupComponent, {
            class: 'gray modal-md',
            initialState: { data: type }
        }))

    search = {
        value: '',
        icon: <boolean>false,
        params: <any>{},
        onSearch: () => {
            this.search.value = this.toSlug._ini(this.search.value.replace(/\s+/g, ' ').trim())
            if (this.search.check_search(this.search.value)) {
                this.search.params.value = this.search.value
                this.search.params.type = ''
                this.router.navigate(['/tim-kiem'], {
                    queryParams: this.search.params,
                    queryParamsHandling: 'merge'
                })
            }
            this.search.value = ''
            this.search.icon = !this.search.icon
        },
        check_search: (value: string) => {
            let skip = true
            skip =
                value.toString().length > 0 &&
                value != '' &&
                value != '-' &&
                value != '[' &&
                value != ']' &&
                value != '\\' &&
                value != '{' &&
                value != '}'
                    ? true
                    : false
            return skip
        },

        showSearch: () => {
            this.search.icon = !this.search.icon
        }
    }
}
