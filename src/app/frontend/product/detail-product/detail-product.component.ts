import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { Meta, Title } from '@angular/platform-browser'
import { ActivatedRoute, Router } from '@angular/router'
import { OwlOptions, SlidesOutputData } from 'ngx-owl-carousel-o'
import { ToastrService } from 'ngx-toastr'
import { Subscription } from 'rxjs'
import { Globals } from '../../../globals'
import { CartService } from '../../../services/apicart/cart.service'
import { ModalSinginSingupComponent } from '../../modal-singin-singup/modal-singin-singup.component'
import { DEFAULT_IMAGE } from '../../core'

@Component({
    selector: 'app-detail-product',
    templateUrl: './detail-product.component.html',
    styleUrls: ['./detail-product.component.scss'],
    providers: [CartService]
})
export class DetailProductComponent implements OnInit, OnDestroy {
    public connect: Subscription
    public data: any = {}
    public price: number = 0
    public link: any
    defaultImage = DEFAULT_IMAGE

    public activeSlides: SlidesOutputData
    imgListLength: number
    public modalRef: BsModalRef

    activeTab

    status_tab: boolean = true
    active_tab: boolean
    @ViewChild('owlLibrary', { static: false }) owlLibrary: any

    public token = {
        product: 'api/products/detail'
    }

    constructor(
        public route: ActivatedRoute,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals,
        public apiCart: CartService,
        private title: Title,
        private meta: Meta,
        public modalService: BsModalService
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getProductDetail':
                    this.data = res.data

                    this.title.setTitle(this.data.name)
                    this.meta.updateTag({
                        name: 'description',
                        content: this.data.description
                    })

                    setTimeout(() => {
                        this.globals.htmlRender()
                    }, 500)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.link = params.link || ''

            this.globals.send({
                path: this.token.product,
                token: 'getProductDetail',
                params: { link: this.link }
            })
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    handleDisplayTab = (status: boolean) => (this.status_tab = status)

    enableFormRegisterCourse = (type: number) =>
        (this.modalRef = this.modalService.show(ModalSinginSingupComponent, {
            class: 'gray modal-md',
            initialState: { data: type }
        }))
}
